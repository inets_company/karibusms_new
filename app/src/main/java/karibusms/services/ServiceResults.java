package karibusms.services;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;
import karibusms.application.MyApplication;
import karibusms.callback.ResultsLoadedListener;
import karibusms.database.SmsStore;
import karibusms.info.SmsList;
import karibusms.logging.L;
import karibusms.task.TaskLoadphones;
import me.tatarka.support.job.JobParameters;
import me.tatarka.support.job.JobService;

/**
 * Created by Yohana on 2/22/2016.
 */
public class ServiceResults extends JobService implements ResultsLoadedListener {
    private JobParameters jobParameters;
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
       // L.t(this, "onStartJob");
        this.jobParameters = jobParameters;
        new TaskLoadphones(this).execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
       // L.t(this, "onStopJob");
        return false;
    }
    @Override
    public void onResultsloadedListener(ArrayList<SmsList> listPhones) {

        if(listPhones.size()==0){
        } else {
        for (int i = 0; i < listPhones.size(); i++) {
            L.t(this, listPhones.get(i).getLink());
            //this sendSms that are Grouped in a category
            sendSMS(listPhones.get(i).getPhone(),listPhones.get(i).getMsg());
            //saves the Sms to local Application Database
            MyApplication.getWritableDatabase().deleteFromlist(listPhones.get(i).getSid());
        }}
        jobFinished(jobParameters, false);
    }

    public void sendSMS(String phoneNumber, String message) {
        try {
            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> mSMSMessage = sms.divideMessage(message);
//            for (int i = 0; i < mSMSMessage.size(); i++) {
//                sentPendingIntents.add(i, sentPI);
//                deliveredPendingIntents.add(i, deliveredPI);
//            }
            sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage,null,null);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "SMS sending failed...",Toast.LENGTH_SHORT).show();
        }
    }
}
