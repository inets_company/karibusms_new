package karibusms.services;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import karibusms.R;
import karibusms.extras.Utils;
import karibusms.info.SMS;


public class SMSSendingService extends IntentService {

    public static final String ACTION_SENT="com.karibusms.sms_sent";
    public static final String ACTION_DELIVERED="com.karibusms.sms_delivery";
    private static long SENDING_INTERVAL_TIME_MILLS = 9000;
    private static  boolean IS_SCHEDULED = false;
    public static  boolean METHOD_SEND_THEN_WAIT = false;

    public SMSSendingService() {
        super("SMSSendingService");
        try {
            SENDING_INTERVAL_TIME_MILLS = Long.parseLong(Utils.readPreferences(this,"SMS_SENDING_INTERVAL", String.valueOf(SENDING_INTERVAL_TIME_MILLS)));
        }catch (Exception ignored){
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Realm realm = Realm.getDefaultInstance();
            if(realm.where(SMS.class).equalTo("status", SMS.PENDING_TO_CLIENT).count()>0){
                justSchedule();
            }
            realm.close();
        }catch (Exception ignored){

        }
    }

    @SuppressLint("WakelockTimeout")
    @Override
    protected void onHandleIntent(Intent intent) {
        showNotification("KaribuSMS","Currently sending SMSes");

        IS_SCHEDULED = false;
        //cancelSchedule();

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getPackageName()+":lock");

        wakeLock.acquire();

        sendAll();

        wakeLock.release();

        Log.d("SMSSending","Done sending");
    }

    private void sendThenWaitForStatus(){
        Realm realm = Realm.getDefaultInstance();
        SMS sms = realm.where(SMS.class).equalTo("status", SMS.PENDING_TO_CLIENT)
                .findAllSorted("time", Sort.ASCENDING).first(null);
        if (sms!=null){
            send(realm, sms);
            justSchedule();
        }else{
            clean(realm);
        }

        realm.close();
    }

    private void sendAll(){
        Realm realm = Realm.getDefaultInstance();
        for (SMS sms : realm.where(SMS.class).equalTo("status", SMS.PENDING_TO_CLIENT)
                .findAllSorted("time", Sort.ASCENDING)) {
            sendAndWait(realm,sms);
        }

        clean(realm);
        retryFailed(realm);

        realm.close();

        rescheduleFailed(this);
        stopForeground(true);
    }


    private void clean(Realm realm) {
        double count =realm.where(SMS.class).equalTo("status",SMS.SENDING).count();
        if(count>0){
            realm.beginTransaction();
            for (SMS sms : realm.where(SMS.class).equalTo("status", SMS.SENDING).lessThan("timeSent",(long)  (System.currentTimeMillis()-((1000*60*((1d/14d)*count)+10d)))).findAll()) {
                if (sms.getRetriesCount() > 2) {
                    sms.setStatus(SMS.FAILED_AFTER_MAX_RETRIES);
                }else{
                    sms.setStatus(SMS.FAILED_SENT);
                    sms.setRetriesCount(sms.getRetriesCount() + 1);
                }
                realm.copyToRealmOrUpdate(sms);
            }
            realm.commitTransaction();
        }
    }

    private void sendAndWait(Realm realm,SMS sms){
        int count = send(realm, sms);
        if (SENDING_INTERVAL_TIME_MILLS > 100)
            try {
                Thread.sleep(SENDING_INTERVAL_TIME_MILLS  + ((SENDING_INTERVAL_TIME_MILLS*(count-1)*3)/10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    private int send(Realm realm, SMS sms) {
        realm.beginTransaction();
        sms.setStatus(SMS.SENDING);
        sms.setRetriesCount(sms.getRetriesCount()+1);
        sms.setTimeSent(System.currentTimeMillis());
        realm.copyToRealmOrUpdate(sms);
        realm.commitTransaction();


        ArrayList<PendingIntent> spis=new ArrayList<>();
        ArrayList<PendingIntent> dpis=new ArrayList<>();

        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> smsList = smsManager.divideMessage(sms.getBody());

        for (int i = 0; i < smsList.size(); i++) {
            Intent intent1=new Intent(ACTION_SENT);
            intent1.putExtra("sms_id",sms.getId());
            spis.add(PendingIntent.getBroadcast(this, i+(int) System.currentTimeMillis(),intent1, PendingIntent.FLAG_UPDATE_CURRENT));

            Intent intent2=new Intent(ACTION_DELIVERED);
            intent2.putExtra("sms_id",sms.getId());
            dpis.add(PendingIntent.getBroadcast(this,i+(int) System.currentTimeMillis(),intent2, PendingIntent.FLAG_UPDATE_CURRENT));
        }

        smsManager.sendMultipartTextMessage(sms.getPhoneNumber(),null, smsList, spis, dpis);
        return smsList.size();
    }


    private void retryFailed(Realm realm) {
        RealmResults<SMS> smses=realm.where(SMS.class).equalTo("status",SMS.FAILED_SENT).findAll();
        for (SMS sms :smses) {
            sendAndWait(realm,sms);
        }
    }


    public static void rescheduleFailed(Context context){
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(SMS.class)
                .equalTo("status",SMS.FAILED_SENT)
                .or()
                .equalTo("status",SMS.SENDING)
                .or()
                .equalTo("status",SMS.PENDING_TO_CLIENT).count()>0 && !IS_SCHEDULED){
            int seconds=60;
            Intent intent = new Intent(context, SMSSendingService.class);
            intent.setAction("SEND_FAILED");
            PendingIntent pendingIntent = PendingIntent.getService(context,  0,  intent, PendingIntent.FLAG_CANCEL_CURRENT);
            long trigger = System.currentTimeMillis() + (seconds*1000);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, pendingIntent);
            IS_SCHEDULED = true;
        }
        realm.close();
    }

    private void justSchedule(){
        if (!IS_SCHEDULED){
            int seconds=60;
            Intent intent = new Intent(this, SMSSendingService.class);
            intent.setAction("SEND_FAILED");
            PendingIntent pendingIntent = PendingIntent.getService(this,  0,  intent, PendingIntent.FLAG_CANCEL_CURRENT);
            long trigger = System.currentTimeMillis() + (seconds*1000);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, pendingIntent);
            IS_SCHEDULED = true;
        }
    }

    private void cancelSchedule(){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, SMSSendingService.class);
        intent.setAction("SEND_FAILED");
        PendingIntent pendingIntent = PendingIntent.getService(this,  0,  intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);
    }


    private void showNotification(String title, String message) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false).setPriority(NotificationCompat.PRIORITY_HIGH);
        startForeground(666849, notificationBuilder.build());
    }
}
