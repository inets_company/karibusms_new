package karibusms.services;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import io.realm.Realm;
import karibusms.R;
import karibusms.activities.NotificationViewerActivity;
import karibusms.database.SmsStore;
import karibusms.extras.AppConfig;
import karibusms.extras.Utils;
import karibusms.info.PendingRequest;
import karibusms.network.VolleySingleton;

public class KaribusmsGCMListenerService extends GcmListenerService {


    SmsStore.KaribusmsdbHelper karibusmsdbHelper;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.v("GCM",data.toString());
        String phones = data.getString("Notice");
        if (phones.equalsIgnoreCase("1")) {
            String message = data.getString("message");
            String title = data.getString("title");
            sendNotification(title, message);
        } else {
            try {
                JSONObject jsonData = new JSONObject(phones);
                parseData(jsonData);
            } catch (JSONException e) {

            }
        }
    }

    /**
     * Create and show a notification containing the received GCM message.
     *
     * @param title   the title of the message received
     * @param message GCM message received
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, NotificationViewerActivity.class);
        intent.putExtra("content_to_show", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(666899, notificationBuilder.build());
    }

    private void parseData(JSONObject response) {

        karibusmsdbHelper = new SmsStore.KaribusmsdbHelper(this);

        String msg;
        String link;
        String downloadLink = "";
        if (response != null && response.length() > 0) {
            try {
                if (response.has("action")
                        && response.getString("action").equalsIgnoreCase("PULL_SMS_TO_SEND")){
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    PendingRequest pr=new PendingRequest(PendingRequest.TYPE_CHECK_NEW_SMS_TO_SEND,System.currentTimeMillis(),PendingRequest.STATUS_PENDING);
                    realm.copyToRealmOrUpdate(pr);
                    realm.commitTransaction();
                    realm.close();
                    startService(new Intent(this,SMSRequestHandlerService.class));
                }else if (response.has("action")
                        && response.getString("action").equalsIgnoreCase("REPORT_ONLINE_PRESENCE")){
                    sayHello();
                }else if (response.has("action")
                        && response.getString("action").equalsIgnoreCase("CHANGE_SMS_SENDING_RATE")){
                    if (response.has("extra") &&
                            response.getJSONObject("extra").optDouble("sending_rate_mpm",0)!=0){
                        double mpm = response.getJSONObject("extra").getDouble("sending_rate_mpm");
                        double interval= 60d*1000d/mpm;
                        Utils.savePreferences(this,"SMS_SENDING_INTERVAL", String.valueOf(interval));
                    }
                }else{
                    msg = response.getString("message");
                    link = response.getString("link");
                    String phonenumber = response.getString("phone_number");
                    if (link.equals("")) {
                        link = downloadLink;
                    }

                    sendNotification("karibusms", "messages are sent from www.karibusms.com");
                    sendSMS(phonenumber, msg);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject  sayHello() {
        String url = AppConfig.sayHello( Utils.readPreferences(this,"business_id",""));
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null,future,future);
        VolleySingleton.getInstance().getRequestQueue().add(request);

        Log.v("Report online url",url);


        JSONObject response = null;
        try {
            response = future.get();
        } catch (InterruptedException e) {
            // exception handling
        } catch (ExecutionException e) {
            // exception handling
        }

        Log.v("Report online response",response!=null?response.toString():"");
        return response;
    }

    public void sendSMS(String phoneNumber, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        if (message.length() < 160) {
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
        } else {
            ArrayList<String> parts = smsManager.divideMessage(message);
            smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null,
                    null);
        }
    }

}