package karibusms.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import io.realm.Realm;
import io.realm.RealmResults;
import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.Utils;
import karibusms.info.PendingRequest;
import karibusms.info.SMS;
import karibusms.network.VolleySingleton;

public class SMSRequestHandlerService extends IntentService {

    private static final String TAG = SMSRequestHandlerService.class.getSimpleName();

    public static int retries=0;

    public SMSRequestHandlerService() {
        super("SMS Handler Service");
    }

    @Override
    public void onCreate() {
        super.onCreate();
       // showNotification("KaribuSMS","Synchronization in progress");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (Utils.isNetworkAvailable(this)){
            Realm realm = Realm.getDefaultInstance();
            do{
                SMS sms=realm.where(SMS.class).equalTo("status",SMS.PENDING_TO_SERVER).findFirst();
                PendingRequest pr=realm.where(PendingRequest.class).equalTo("status",PendingRequest.STATUS_PENDING)
                        .equalTo("type",PendingRequest.TYPE_CHECK_NEW_SMS_TO_SEND).findFirst();

                PendingRequest prReport=realm.where(PendingRequest.class).equalTo("status",PendingRequest.STATUS_PENDING)
                        .equalTo("type",PendingRequest.TYPE_SEND_REPORT).findFirst();
                try{
                    if (sms!=null ){
                        //uncheck sending sms to server
                        //sendSMSToServer(realm, sms);
                    }else {
                        try {
                            if(checkPendingFromServer(realm, pr)
                                    && sendReportToServer(realm,prReport)){
                                realm.close();
                                haltService();
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }while(true);
        }else{
            rescheduleService(20);
        }
        haltService();
    }

    private void haltService() {
        stopForeground(true);
        stopSelf();
    }

    private boolean checkPendingFromServer(Realm realm, PendingRequest pr) throws JSONException {
        if (pr==null){
            Log.d(TAG,"No Pulling request");
            return true;
        }
        Log.d(TAG,"Pulling messages");
        String url = AppConfig.pullSMS( Utils.readPreferences(this,"business_id",""));
        JSONObject res = httpRequest(url);
        if (res!=null){
            retries=0;
            Log.d(TAG,res.toString());
            JSONArray messages = res.getJSONArray("data");
            if (res.optDouble("sending_rate_mpm",0)!=0){
                double mpm = res.getDouble("sending_rate_mpm");
                double interval= 60d*1000d/mpm;
                Utils.savePreferences(this,"SMS_SENDING_INTERVAL", String.valueOf(interval));
            }
            realm.beginTransaction();
            for (int i = 0; i < messages.length(); i++) {
                String phoneNum = messages.getJSONObject(i).getString("phone_number");
                String msg = messages.getJSONObject(i).getString("content");
                SMS sms = new SMS(phoneNum,msg, System.currentTimeMillis(),SMS.PENDING_TO_CLIENT);
                sms.setId(messages.getJSONObject(i).getString("pending_sms_id"));
                realm.copyToRealmOrUpdate(sms);
            }
            pr.deleteFromRealm();
            realm.commitTransaction();
            Intent intent2 = new Intent(this, SMSSendingService.class);
            intent2.setAction("SEND_PENDING");
            startService(intent2);
        }else{
            rescheduleService(30);
            Log.e(TAG,"Failed request (Sending SMS to the server)");
        }

        return true;
    }

    private boolean sendReportToServer(Realm realm, PendingRequest pr) throws JSONException {
        Log.d(TAG,"sendReportToServer");

        RealmResults<SMS> all = realm.where(SMS.class)
                .equalTo("status", SMS.SENT)
                .or()
                .equalTo("status", SMS.DELIVERED)
                .or()
                .equalTo("status", SMS.FAILED_DELIVERY)
                .or()
                .equalTo("status", SMS.FAILED_AFTER_MAX_RETRIES)
                .findAll();

        Iterator<SMS> smses = all.iterator();
        JSONArray jsonData = new JSONArray();

        ArrayList<SMS> reported = new ArrayList<>();

        int count = 0;
        while (smses.hasNext()){
            SMS sms = smses.next();
            int status;
            switch (sms.getStatus()){
                case 4: status = 3; break;
                case 10: status = 4; break;
                default: status = sms.getStatus(); break;
            }

            jsonData.put(new JSONObject()
                    .put("pending_sms_id",sms.getId())
                    .put("status",status)
            );
            reported.add(sms);

            count ++;
            if (count>30) break;
        }

        if (jsonData.length()==0){
            if (pr!=null && pr.isValid()){
                realm.beginTransaction();
                pr.deleteFromRealm();
                realm.commitTransaction();
            }
            return true;
        }

        String data = jsonData.toString();
        String url = AppConfig.sendSMSReports( Utils.readPreferences(this,"business_id",""),data);
        JSONObject res = httpRequest(url);

        if (res!=null){
            retries=0;
            realm.beginTransaction();
            if (pr!=null && pr.isValid()){
                pr.deleteFromRealm();
            }

            for (SMS sms : reported) {
                if (sms.getStatus()==SMS.SENT){
                    sms.setStatus(SMS.SENT_SERVER_KNOWS);
                    realm.copyToRealmOrUpdate(sms);
                }else if (sms.isValid()){
                    sms.deleteFromRealm();
                }
            }

            realm.commitTransaction();
            return reported.size()==all.size();
        }else{
            rescheduleService(15);
            Log.e(TAG,url + "Failed request (Sending SMS to the server)");
            return  true;
        }
    }

    private boolean sendSMSToServer(Realm realm, SMS sms) {
        Log.d(TAG,"sendSMSToServer");
        //String body=sms.getBody();
        String body="Feedback Sms here";

        String url = AppConfig.sendFeedbackSMS(
                sms.getPhoneNumber(),
                body,
                Utils.readPreferences(this,"business_id","")
        );
        JSONObject res = httpRequest(url);
        if (res!=null){
            retries=0;
            if (sms.isValid()){
                realm.beginTransaction();
                sms.deleteFromRealm();
                realm.commitTransaction();
            }
            return true;
        }else{
            rescheduleService(15);
            Log.e(TAG,"Failed request (Sending SMS to the server)");
            return false;
        }
    }

    private void rescheduleService(int seconds) {
        retries++;
        Log.v(TAG,"Reschedule");
        Intent intent = new Intent(this, SMSRequestHandlerService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this,  0,  intent, 0);
        long trigger = System.currentTimeMillis() + (seconds*retries*1000);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, pendingIntent);
    }


    private void showNotification(String title, String message) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        startForeground(666899, notificationBuilder.build());
    }


    private JSONObject  httpRequest(String url) {
        Log.d(TAG,"request to "+url);

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null,future,future);
        VolleySingleton.getInstance().getRequestQueue().add(request);

        JSONObject response = null;
        try {
            response = future.get();
        } catch (InterruptedException e) {
            // exception handling
        } catch (ExecutionException e) {
            // exception handling
        }
        Log.d(TAG,"response: "+response);

        return response;
    }
}
