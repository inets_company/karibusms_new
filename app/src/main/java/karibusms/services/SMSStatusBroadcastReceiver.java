package karibusms.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;


import io.realm.Realm;
import karibusms.info.PendingRequest;
import karibusms.info.SMS;

import static android.app.Activity.RESULT_OK;

public class SMSStatusBroadcastReceiver extends BroadcastReceiver {

    private static String TAG="SMSSendingStatus";
    boolean commitNow = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Realm realm = Realm.getDefaultInstance();
        SMS sms = realm.where(SMS.class).equalTo("id", intent.getStringExtra("sms_id")).findFirst();

       /* if (realm.isInTransaction()) {
            realm.commitTransaction();
            realm.close();
            Log.d("PREVIOUS RANSACTION ", "PREVIOUS TRANSACTION STARTED");
            return;
        }  else {*/
       if (realm.isInTransaction()) {
           realm.commitTransaction();
           realm.close();
           Log.d("PREVIOUS RANSACTION ", "PREVIOUS TRANSACTION STARTED");

        } else{

           realm.beginTransaction();
       }
           // Log.d("NEW RANSACTION ", "NEW TRANSACTION STARTED");


            if (intent.getAction().equalsIgnoreCase(SMSSendingService.ACTION_SENT) &&
                    SMSSendingService.METHOD_SEND_THEN_WAIT) {
                reportReceivedNotify(context);
            }

            try {
                if (getResultCode() == RESULT_OK) {
                    sms.setStatus(intent.getAction().equalsIgnoreCase(SMSSendingService.ACTION_SENT) ? SMS.SENT : SMS.DELIVERED);
                } else {
                    sms.setStatus(intent.getAction().equalsIgnoreCase(SMSSendingService.ACTION_SENT) ? SMS.FAILED_SENT : SMS.FAILED_DELIVERY); //fixme: something is wrong here
                    if (intent.getAction().equalsIgnoreCase(SMSSendingService.ACTION_SENT)) {
                        SMSSendingService.rescheduleFailed(context);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                realm.close();
                return;
            }

            Log.d(TAG, "sms sending status:" + sms.getStatus() + " Result " + getResultCode());
            if (sms.getRetriesCount() > 2) {
                sms.setStatus(SMS.FAILED_AFTER_MAX_RETRIES);
            }

            realm.copyToRealmOrUpdate(sms);

            if (realm.where(SMS.class)
                    .equalTo("status", SMS.SENT)
                    .or().equalTo("status", SMS.DELIVERED)
                    .or().equalTo("status", SMS.FAILED_AFTER_MAX_RETRIES).count() > 29
                    || realm.where(SMS.class).equalTo("status", SMS.PENDING_TO_CLIENT).count() == 0) {
                //start service to send status to the server
                PendingRequest pr = new PendingRequest(PendingRequest.TYPE_SEND_REPORT, System.currentTimeMillis(), PendingRequest.STATUS_PENDING);
                realm.copyToRealmOrUpdate(pr);

                context.startService(new Intent(context, SMSRequestHandlerService.class));
            }
            realm.commitTransaction();
            realm.close();

        //}

    }


    private void reportReceivedNotify(Context context){
        Intent intent2 = new Intent(context, SMSSendingService.class);
        intent2.setAction("SEND_PENDING");
        context.startService(intent2);
    }
}
