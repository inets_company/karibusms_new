package karibusms.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.Utils;
import karibusms.network.VolleySingleton;


public class GCMRegistrationIntentService extends IntentService {
    String topics[]={"global"};

    public GCMRegistrationIntentService() {
        super("GCMRegistrationIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            //get token
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Utils.savePreferences(getApplicationContext(),"token",token);
            Log.d("TOKEN", token);
            //Subscribe to default_bg topics, we only have one topic tho
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            for (String topic : topics) {
                pubSub.subscribe(token, "/topics/" + topic, null);
            }
            //Send any registration to app's servers.
            sendRegistrationToServer(token);
        } catch (Exception e) {
            sharedPreferences.edit().putBoolean("SENT_TOKEN_TO_SERVER", false).apply();
        }
        //Notify whoever may be interested that registration has completed
        Intent registrationComplete = new Intent("GCM_REGISTRATION_COMPLETE");
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        Utils.savePreferences(getApplicationContext(),"token",token);
        Log.d("CAG", "GCM Registration Token: " + token);

        if (Utils.readPreferences(this,"business_id","").isEmpty()){
            return;
        }

        String url = AppConfig.updateGCMToken(Utils.readPreferences(this,"business_id",""),token);
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null,future,future);
        VolleySingleton.getInstance().getRequestQueue().add(request);

        JSONObject response = null;
        try {
            response = future.get();
        } catch (InterruptedException e) {
            // exception handling
        } catch (ExecutionException e) {
            // exception handling
        }

        if (response!=null){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putBoolean("SENT_TOKEN_TO_SERVER", true).apply();
        }
    }
}
