package karibusms.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import io.realm.Realm;
import karibusms.extras.Utils;
import karibusms.info.SMS;

/**
 *Broadcast receiver for SMS_RECEIVED and ACTION_BOOT_COMPLETED
 */
public class BroadcastReceiverImpl extends BroadcastReceiver {

    private final static String ACTION_SMS_RECEIVED ="android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("SMSBroadcastReceiver","Broadcast receiver has something to do");

        if(intent.getAction().equals(ACTION_SMS_RECEIVED)){
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs;
            String from="";
            StringBuilder smsBody=new StringBuilder();
            if (bundle != null){
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        from = msgs[i].getOriginatingAddress();
                        if (!Utils.isValidPhoneNumber(from)){
                           return;
                        }
                        smsBody.append(msgs[i].getMessageBody());
                    }
                    //We need uniformity in storing phone numbers, start with 0. we are not dealing with other countries
                    from=from.replaceAll("^255|^\\+255","0");
                    SMS sms = new SMS(from,smsBody.toString(), System.currentTimeMillis(), SMS.PENDING_TO_SERVER);
                        Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.insert(sms);
                    realm.commitTransaction();
                    realm.close();
                    context.startService(new Intent(context,SMSRequestHandlerService.class));
                }catch(Exception e){
                   e.printStackTrace();
                }
            }
        }else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Realm realm = Realm.getDefaultInstance();
            context.startService(new Intent(context,SMSRequestHandlerService.class));
            if (realm.where(SMS.class).equalTo("status",SMS.PENDING_TO_CLIENT).count()>0){
                Intent intent2 = new Intent(context, SMSSendingService.class);
                intent2.setAction("SEND_PENDING");
                context.startService(intent2);
            }else if (realm.where(SMS.class).equalTo("status",SMS.FAILED_SENT).count()>0){
                Intent intent2 = new Intent(context, SMSSendingService.class);
                intent2.setAction("SEND_FAILED");
                context.startService(intent2);
            }
            realm.close();
        }
    }
}
