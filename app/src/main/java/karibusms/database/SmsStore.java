package karibusms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import karibusms.info.SmsList;
import karibusms.logging.L;

import static android.content.ContentValues.TAG;

/**
 * Created by Yohana on 4/22/2016.
 */
public class SmsStore  {
    public static final int MESSAGE_STATUS_UNKNOWN = 0;
    public static final int MESSAGE_STATUS_PENDING = 1;
    public static final int MESSAGE_STATUS_SENT = 2;
    public static final int MESSAGE_STATUS_DELIVERED = 3;
    public static final int MESSAGE_STATUS_REQUEUED = 4;
    public static final int MESSAGE_STATUS_FAILED = 10;

    private KaribusmsdbHelper mHelper;
    private SQLiteDatabase nDatabase;

    public SmsStore(Context contex) {
        mHelper = new KaribusmsdbHelper(contex);
        nDatabase = mHelper.getWritableDatabase();
    }


    public void insertGroupMessage(ArrayList<String> listPhones, String msg, String link, boolean
            clearPrevious, String
            table) {

        if (clearPrevious) {
            deletePhones();
        }
        //create a sql prepared statement
        String sql = "INSERT INTO " + (table) + " VALUES (?,?,?,?,?);";
        //compile the statement and start a transaction
        Log.d("KARIBUSMS", sql);
        SQLiteStatement statement = nDatabase.compileStatement(sql);
        try {
            nDatabase.beginTransaction();
            for (int i = 0; i < listPhones.size(); i++) {

                statement.clearBindings();
                String phone = listPhones.get(i);
                //for a given column index, simply bind the data to be put inside that index
                statement.bindString(2, phone);
                statement.bindString(3, msg);
                statement.bindString(4, link);
                statement.bindString(5, "0");
                statement.execute();
            }
            nDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            L.m(e.toString());
        } finally {
            nDatabase.endTransaction();
        }


    }
    public void deletePhones() {
        nDatabase.delete(KaribusmsdbHelper.TABLE_KARIBUSMS, null, null);
    }
    public void deleteFromlist(Integer id) {
        String[] column = {
                id.toString()
        };

        nDatabase.delete(KaribusmsdbHelper.TABLE_KARIBUSMS, "id=?", column);
       // L.m("PHONE DELETED");
    }
    public ArrayList<SmsList> perseSmsinfo() {
        ArrayList<SmsList> numbers=new ArrayList<>();

        //get a content of columns to be retrieved
        String[] columns = {KaribusmsdbHelper.COLUMN_UID,
                KaribusmsdbHelper.COLUMN_PHONE,
                KaribusmsdbHelper.COLUMN_MESSAGE,
                KaribusmsdbHelper.COLUMN_STATUS,
                KaribusmsdbHelper.COLUMN_LINK,
        };

        String[] column = {
                "0"
        };
        // nDatabase.
        Cursor cursor = nDatabase.query((KaribusmsdbHelper.TABLE_KARIBUSMS),columns, "status=?",
                column,null,null,
                null);
        if (cursor != null && cursor.moveToFirst()) {

            do {
                SmsList smsphonelist=new SmsList();
                Integer  phone_id= cursor.getInt(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_UID));
                String  phone_number= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_PHONE));
                String  phone_msg= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_MESSAGE));
                String  phone_link= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_LINK));
                smsphonelist.setSid(phone_id);
                smsphonelist.setPhone(phone_number);
                smsphonelist.setLink(phone_link);
                smsphonelist.setMsg(phone_msg);
                numbers.add(smsphonelist);
            } while (cursor.moveToNext());
        }
        return numbers;
    }

    public static class KaribusmsdbHelper extends SQLiteOpenHelper {
        public static final String TABLE_KARIBUSMS = "karibusms";
        public static final String COLUMN_UID = "id";
        public static final String COLUMN_PHONE = "phonenumber";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_SYNCED_STATUS = "synced_status";
        public static final String COLUMN_LINK = "msg_link";

        private static final String CREATE_TABLE_KARIBUSMS = "CREATE TABLE " + TABLE_KARIBUSMS + "(" +
                COLUMN_UID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_PHONE + " TEXT," +
                COLUMN_MESSAGE + " TEXT," +
                COLUMN_LINK + " TEXT," +
                COLUMN_STATUS + " INTEGER," +
                COLUMN_SYNCED_STATUS + " INTEGER" +
                ");";


        //create another table here
        private static final String DB_NAME = "karibusms_db";
        private static final int DB_VERSION = 1;

        private Context mContext;

        public KaribusmsdbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            mContext = context;
        }


        public void saveSms(String phoneNo, String smsmessage, String status){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_PHONE, phoneNo);
            values.put(COLUMN_MESSAGE, smsmessage);
            values.put(COLUMN_STATUS, status);
            values.put(COLUMN_SYNCED_STATUS, MESSAGE_STATUS_UNKNOWN);
            long id = db.insert(TABLE_KARIBUSMS, null, values);
            db.close();

            Log.d(TAG, "sms inserted" + id);

        }

        public Cursor getAllSms() {
            SQLiteDatabase db = this.getReadableDatabase();
            String query= "select * from "+TABLE_KARIBUSMS;
            return db.rawQuery(query,null);
        }

            // here all sms are being updated to check for delivery status
        public void updateSms(String message, String statusii){
            SQLiteDatabase db = this.getWritableDatabase();
             ContentValues values = new ContentValues();
            String sms= message;

            String statuss = statusii;
           // if(statuss == "-1"){
                String statussa= "Delivered";
                values.put(COLUMN_STATUS, statussa);
                String query= "update " + TABLE_KARIBUSMS + " set " + COLUMN_STATUS + " = "+COLUMN_STATUS+" + '" + statussa +
                        "' where "+ COLUMN_MESSAGE+" = ?";
                db.execSQL(query);
                Log.d("sms inserted",""+statussa);
//
//
            db.close();

        }


        public Cursor getMyPhones(){
            SQLiteDatabase db = this.getReadableDatabase();
            //String myQeury= "select " + COLUMN_PHONE + " from " + TABLE_KARIBUSMS + " ";
            String myQeury= "select * from " + TABLE_KARIBUSMS ;
            Cursor res= db.rawQuery(myQeury, null);
            return res;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_KARIBUSMS);
              //  L.m("create table KARIBUSMS  executed");
            } catch (SQLiteException exception) {
               // Log.d("CREATE TABLE", exception + "");
               //  L.T(mContext, exception+"");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(" DROP TABLE " + TABLE_KARIBUSMS + " IF EXISTS;");
                onCreate(db);
            } catch (SQLiteException exception) {
               // L.t(mContext, exception + "");
            }
        }
    }
}

