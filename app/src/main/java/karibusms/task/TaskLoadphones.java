package karibusms.task;

import android.os.AsyncTask;
import java.util.ArrayList;

import karibusms.callback.ResultsLoadedListener;
import karibusms.extras.Utils;
import karibusms.info.SmsList;

/**
 * Created by Yohana on 2/19/2016.
 */
public class TaskLoadphones extends AsyncTask<Void, Void, ArrayList<SmsList>> {
    private ResultsLoadedListener phones;
    public TaskLoadphones(ResultsLoadedListener phones) {
        this.phones=phones;
    }
    @Override
    protected ArrayList<SmsList> doInBackground(Void... params) {
        ArrayList<SmsList> listPhones = Utils.loadPhonenumber();
        return listPhones;
    }
    @Override
    protected void onPostExecute(ArrayList<SmsList> listPhones) {
        if (phones != null) {
            phones.onResultsloadedListener(listPhones);
        }
    }
}
