package karibusms.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import karibusms.R;
import karibusms.activities.BusinessDescription;
import karibusms.activities.Description;
import karibusms.adapters.bn_adapter;
import karibusms.callback.EndlessRecyclerViewScrollListener;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.info.Business;
import karibusms.logging.L;
import karibusms.services.GCMRegistrationIntentService;
import karibusms.widgets.FooterLoadingIndicator;
import karibusms.widgets.LoadingIndicator;

/**
 * Created by Yohana on 5/9/2016.
 */
public class Subscribe extends Fragment implements bn_adapter.clickListener, SwipeRefreshLayout
        .OnRefreshListener {

    //Creating a List of superheroes
    private ArrayList<Business> listBusiness;
    //Creating Views
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private bn_adapter adapter;
    //Volley Request Queue
    private RequestQueue requestQueue;
    private LoadingIndicator loadingIndicator;
    boolean FOOTER_LOADER_STATE_LOADING =false;
    boolean HAS_MORE_CONTENT=true;
    private FooterLoadingIndicator listFooterLoader;
    int PAGE=1;
    int TOTAL_ITEMS_COUNT=0;
    String url;
    ErrorMessage errormsg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams
                .SOFT_INPUT_STATE_HIDDEN);
        View rootView = inflater.inflate(R.layout.subscribe, container, false);
        String token = Utils.readPreferences(getActivity(), "token", "");
        if (token.isEmpty()) {
            Intent itent = new Intent(getActivity(), GCMRegistrationIntentService.class);
            getActivity().startService(itent);
        }
        //Initializing Views
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        loadingIndicator=(LoadingIndicator)rootView.findViewById(R.id.loading_indicator);
        loadingIndicator.setDominantColor(getThemeColor());
        loadingIndicator.getRim().setRimWidth(8);
        loadingIndicator.show();
        listFooterLoader= (FooterLoadingIndicator)rootView. findViewById(R.id.footer_indicator);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_business);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        //recyclerView.setHasFixedSize(false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        String gcm_id=Utils.readPreferences(getActivity(),"token","");
        L.m("GCM ID"+gcm_id);

        //Initializing our superheroes list
        listBusiness = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(getActivity());
        httpRequest();
        adapter = new bn_adapter(getActivity());

        //Adding adapter to recyclerview
        // recyclerView.Setbusiness
        recyclerView.setAdapter(adapter);
        adapter.setbusiness(listBusiness);
        adapter.setItemClick(this);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState==RecyclerView.SCROLL_STATE_IDLE || true){ //true for testing
                    LinearLayoutManager llm=((LinearLayoutManager)recyclerView.getLayoutManager());
                    if (!HAS_MORE_CONTENT){
                        if ((llm.findLastCompletelyVisibleItemPosition()+1)==TOTAL_ITEMS_COUNT ){
                            listFooterLoader.showMsg("No more content");
                        }
                        return;
                    }
                    int lastVisibleItem=llm.findLastVisibleItemPosition();
                    if(TOTAL_ITEMS_COUNT!=0 && (lastVisibleItem+4)>TOTAL_ITEMS_COUNT && !FOOTER_LOADER_STATE_LOADING){
                        PAGE++;
                        L.m("ONSCROLL"+PAGE);
                        FOOTER_LOADER_STATE_LOADING =true;
                        listFooterLoader.showLoader();
                        url = AppConfig.profile("profile",String.valueOf(PAGE));
                        L.m("URL"+url);
                              httpRequest();


                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(),Description.class);
        String b_url=listBusiness.get(position).getImageUrl();
        String business_name=listBusiness.get(position).getName();
        String location=listBusiness.get(position).getLocation();
        String business_id=listBusiness.get(position).getId().toString();
        String descript=listBusiness.get(position).getDescription().toString();
       // L.m("Business ID:"+business_id);
        String business_phone=listBusiness.get(position).getPhone().toString();
       // String phonenumber= Utils.readPreferences(getActivity(),"phone_number","");
        Utils.savePreferences(getActivity(),"business_name",business_name);
        Utils.savePreferences(getActivity(),"business_id",business_id);
        Bundle bundle = new Bundle();
        bundle.putString("b_url", b_url);
        bundle.putString("descript", descript);
        // bundle.putString("b_name", business_name);
        bundle.putString("business_id", business_id);
        bundle.putString("location", location);
        bundle.putString("b_phone", business_phone);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    //JsonArrayRequest of volley
    private void httpRequest() {
    //progressBar.setVisibility(View.VISIBLE);
        url = AppConfig.profile("profile",String.valueOf(PAGE));
        JsonObjectRequest req = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingIndicator.hide();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });

         // add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
       // progressBar.setVisibility(View.GONE);
        PAGE++;
        loadingIndicator.hide();
        FOOTER_LOADER_STATE_LOADING =false;
        L.m("JSON OBJECT" + response.toString());
        if (response != null && response.length() > 0) {
            try {
                JSONArray businessinfo_array = response.getJSONArray("business");
                // L.m("JSON OBJECT"+businessinfo_array.toString());
                for (int i = 0; i < businessinfo_array.length(); i++) {
                    Business business_datail = new Business();
                    JSONObject businessinfo = businessinfo_array.getJSONObject(i);
                    if(businessinfo.getString("url").equalsIgnoreCase("null")){
                        business_datail.setImageUrl("");
                    }else {
                        business_datail.setImageUrl(businessinfo.getString("url"));
                    }
                    business_datail.setName(businessinfo.getString("business_name"));
                    if(businessinfo.getString("location").equalsIgnoreCase("null")){
                        business_datail.setLocation("");
                    }else {
                        business_datail.setLocation(businessinfo.getString("location"));
                    }
                    if(businessinfo.getString("description").equalsIgnoreCase("null") ||
                            businessinfo.getString("description").isEmpty()){
                        business_datail.setDescription("Description not updated yet");
                    }else {
                        business_datail.setDescription(businessinfo.getString("description"));
                    }
                    business_datail.setSubscriber(businessinfo.getString("subscribers"));
                    business_datail.SetId(businessinfo.getString("business_id"));
                    business_datail.setPhone(businessinfo.getString("phone_number"));
                    listBusiness.add(business_datail);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Notifying the adapter that data has been added or changed
            TOTAL_ITEMS_COUNT= recyclerView.getAdapter().getItemCount();
            adapter.notifyDataSetChanged();
        } else {
            HAS_MORE_CONTENT=false;
            listFooterLoader.showMsg("No more content");
        }

    }

    @Override
    public void onRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        L.m("ONREFRESH"+PAGE);
        // url = AppConfig.profile("profile",String.valueOf(PAGE));
        httpRequest();
    }
    private int getThemeColor(){
        return Color.parseColor("#4CAF50");
    }
}
