//package karibusms.fragments;
//
//import android.app.Fragment;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import org.json.JSONException;
//import org.json.JSONObject;
//import java.util.HashMap;
//import karibusms.R;
//import karibusms.extras.AppConfig;
//import karibusms.extras.ErrorMessage;
//import karibusms.extras.Utils;
//import karibusms.logging.L;
//
///**
// * Created by Yohana on 5/9/2016.
// */
//public class statistics extends Fragment implements  View.OnClickListener{
//
//    private RequestQueue requestQueue;
//     String business,business_id;
//    private String owner_phone;
//    Button subscribers_count,balance_count,sms_sent,pending_sms;
//    ProgressBar progressBar;
//    String contact_numbers;
//    TextView business_name;
//    ErrorMessage errormsg;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.statistics_activity, container, false);
//        requestQueue= Volley.newRequestQueue(getActivity());
//        progressBar = (ProgressBar) rootView.findViewById(R.id.uploadBar);
//        subscribers_count = (Button)rootView.findViewById(R.id.subscribers_count);
//        balance_count = (Button)rootView.findViewById(R.id.sms_balance_count);
//        sms_sent = (Button)rootView.findViewById(R.id.sms_sent);
//        business_name=(TextView)rootView.findViewById(R.id.business_name);
//        pending_sms = (Button)rootView.findViewById(R.id.pending_sms);
//        subscribers_count.setOnClickListener(this);
//        balance_count.setOnClickListener(this);
//        sms_sent.setOnClickListener(this);
//        pending_sms.setOnClickListener(this);
//        business=Utils.readPreferences(getActivity(),"business_owner","");
//        owner_phone= Utils.readPreferences(getActivity(),"owner_number","");
//        business_id= Utils.readPreferences(getActivity(),"logged_in_id","");
//        business_name.setText(business);
//        //L.m("BUSINESS NAME"+business_name);
//        String url=AppConfig.statisticUrl("statistics",business_id);
//        httpRequest(url);
//        return rootView;
    //}

//    @Override
//    public void onClick(View v) {
//        switch (v.getId())
//    {
//            //on clicking register button move to Register Activity
//            case R.id.subscribers_count:
//                String url = "http://www.karibusms.com";
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
//                break;
//        case R.id.sms_balance_count:
//          url = "http://www.karibusms.com";
//        i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);
//            break;
//        case R.id.sms_sent:
//             url = "http://www.karibusms.com/";
//          i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);
//            break;
//        case R.id.pending_sms:
//          url = "http://www.karibusms.com";
//           i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);
//            break;
//        }
//    }
//    private void  httpRequest(String url){
//// Post params to be sent to the server
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("tag", "statistics");
//        params.put("business_id", business_id);
//
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                            parseData(response);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressBar.setVisibility(View.GONE);
//                errormsg=new ErrorMessage(getActivity());
//                errormsg.handleVolleyError(error);
//            }
//        });
//
//// add the request object to the queue to be executed
//        requestQueue.add(req);
//    }
//    //This method will parse json data
//    private void parseData(JSONObject response) {
//        progressBar.setVisibility(View.GONE);
//        String subscribers="0";
//        String smssent="0";
//        String smspending="0";
//        String smsremaining="0";
//        if (response != null && response.length() > 0) {
//     try {
//         subscribers = response.getString("subscribers");
//         smssent=response.getString("sentsms");
//         smspending=response.getString("pendingsms");
//         smsremaining=response.getString("remainingsms");
//     } catch (JSONException e) {
//         e.printStackTrace();
//     }
//
//            if(smssent.equalsIgnoreCase("null")){
//                sms_sent.setText("0");
//            }else {
//                sms_sent.setText(smssent);
//            }
//            if(subscribers.equalsIgnoreCase("null")){
//                subscribers_count.setText("0");
//            }else {
//                subscribers_count.setText(subscribers);
//            } if(smspending.equalsIgnoreCase("null")){
//                pending_sms.setText("0");
//            }else {
//                pending_sms.setText(smspending);
//            }
//            if(smsremaining.equalsIgnoreCase("null")){
//                balance_count.setText("0");
//            }else {
//                balance_count.setText(smsremaining);
//            }
//
//        }
//
//
//    }


//}
