package karibusms.fragments;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import karibusms.R;
import karibusms.application.MyApplication;
import karibusms.extras.AppConfig;
import karibusms.extras.Utils;

/**
 * Created by joshuajohn on 02/09/16.
 */
public class Profile extends Fragment {

    private View view;
    private TextView tv_first_name, tv_email, tv_mobile_phone, tv_city, tv_country;
    private String firstName="";
    private String emailAddress="";
    private String mobileNumber="";
    private String city="";
    private String country="";
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile, container, false);

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");

        initializeProfileView();
        getUserProfile();
        return view;
    }

    private void initializeProfileView() {
        tv_first_name = (TextView) view.findViewById(R.id.first_name);
        tv_email = (TextView) view.findViewById(R.id.email);
        tv_mobile_phone = (TextView) view.findViewById(R.id.mobile_number);
        tv_city = (TextView) view.findViewById(R.id.city);
        tv_country = (TextView) view.findViewById(R.id.country);

        // Font path
        String fontPath = "fonts/Lato-Regular.ttf";
        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        tv_first_name.setTypeface(tf);
        tv_email.setTypeface(tf);
        tv_mobile_phone.setTypeface(tf);
        tv_city.setTypeface(tf);
        tv_country.setTypeface(tf);
    }

    private void getUserProfile(){

        requestQueue= Volley.newRequestQueue(getActivity());
        String business_id= Utils.readPreferences(getActivity(),"business_id","");
        final String profile_url= AppConfig.userProfile("get_profile",business_id);
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(profile_url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                for (int i=0; i<response.length(); i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        firstName=object.getString("business_name");
                        emailAddress=object.getString("email");
                        mobileNumber=object.getString("phone_number");
                        city=object.getString("city");
                        country=object.getString("country");
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    tv_first_name.setText("NAME\n"+firstName);
                    tv_email.setText("EMAIL\n"+emailAddress);
                    tv_mobile_phone.setText("MOBILE\n"+mobileNumber);
                    tv_city.setText("CITY\n"+city);
                    tv_country.setText("COUNTRY\n"+country);

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                   progressDialog.dismiss();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
    }
}
