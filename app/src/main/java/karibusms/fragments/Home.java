package karibusms.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import karibusms.R;

import karibusms.dashboard.Dashboard;

import karibusms.dashboard.DashboardRecyclerViewAdapter;
import karibusms.database.SmsStore;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.info.PendingRequest;
import karibusms.services.SMSRequestHandlerService;

/**
 * Created by joshuajohn on 02/09/16.
 */
public class Home extends Fragment {
    View view;
    private RecyclerView recyclerView;
    private SessionManager session;
    private Toolbar toolbar;
    private GridLayoutManager gridLayoutManager;
    private DashboardRecyclerViewAdapter dashboardRecyclerViewAdapter;

    private List<Dashboard> items;
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    private static final int PERMISSION_REQUEST_CONTACT = 100;

    SmsStore.KaribusmsdbHelper karibusmsdbHelper;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.home, container, false);
        session=new SessionManager(getActivity());
        initializeViews();

            // Gives RunTime Permission on reandig and sending of SMS
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
            requestNecessaryPermissions();
        }
        return view;
    }

    public Home(){

    }

        private void initializeViews(){
        List<Dashboard> dashItems=getDashboard();
        gridLayoutManager=new GridLayoutManager(getActivity(),2);
        recyclerView=(RecyclerView)view.findViewById(R.id.dashboard_recycler_view);
        recyclerView.setLayoutManager(gridLayoutManager);
            
        dashboardRecyclerViewAdapter=new DashboardRecyclerViewAdapter (dashItems,getActivity());
        recyclerView.setAdapter(dashboardRecyclerViewAdapter);
    }

        private  List<Dashboard> getDashboard(){
        items=new ArrayList<>();
            items.add(new Dashboard(R.drawable.sms_icon_update,"MESSAGING"));
            items.add(new Dashboard(R.drawable.addressbook_icon_update,"CONTACTS"));
            items.add(new Dashboard(R.drawable.statistics_icon_update,"STATISTICS"));
            items.add(new Dashboard(R.drawable.contcateg_update,"CONTACT CATEGORY"));
            items.add(new Dashboard(R.drawable.chat_update,"FAQ"));

        return items;

    }

    public void requestNecessaryPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_SMS)) {
                Toast.makeText(getContext(), "Please allow permission!", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.READ_SMS,Manifest.permission.WRITE_CONTACTS, Manifest.permission.CALL_PHONE},
                    READ_SMS_PERMISSIONS_REQUEST);
            requestPermissions(
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSION_REQUEST_CONTACT);
        }
    }

}

