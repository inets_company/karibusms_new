package karibusms.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import karibusms.R;
import karibusms.database.SmsStore;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;


public class SendSingleMessage extends Fragment implements View.OnClickListener{
    private View view;
    private EditText etPhoneNumbers,etMessage;
    private String phoneNumbers,content,business_id,typeMessageCat,messageType;
    private Button btnSendSMS;
    private RadioGroup msgTypeGroupId;
    private int messageOption;
    private RadioButton radioButton;
    private ProgressDialog progressDialog;
    private ErrorMessage errormsg;
    private RequestQueue requestQueue;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_send_single_message, container, false);
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Sending SMS...");
        progressDialog.setIndeterminate(false);
        initializeView();


        return view;
    }


    private void initializeView(){
        requestQueue= Volley.newRequestQueue(getActivity());
        etMessage=(EditText)view.findViewById(R.id.text_message);
        etPhoneNumbers=(EditText)view.findViewById(R.id.phone_numbers);
        btnSendSMS=(Button)view.findViewById(R.id.send_single_sms_btn);
        business_id= Utils.readPreferences(getActivity(),"business_id","");
        msgTypeGroupId=(RadioGroup)view.findViewById(R.id.radio_type);
        btnSendSMS.setOnClickListener(this);


    }

        // function for sendSMS button
    @Override
    public void onClick(View v) {


          content=etMessage.getText().toString();
          phoneNumbers=etPhoneNumbers.getText().toString();
        
          messageOption= msgTypeGroupId.getCheckedRadioButtonId();
          radioButton=(RadioButton)view.findViewById(messageOption);
        if(content.length()==0 ) {
            etMessage.setError("Message is required");
            return;
        }
        else if(phoneNumbers.length()==0 || phoneNumbers.length()<10){
            etPhoneNumbers.setError("Invalid Phone Number");
        }

            else if (messageOption < 0) {
                Toast.makeText(getActivity(), "Please select type of message", Toast.LENGTH_LONG).show();
                return;
            }
        else {
            typeMessageCat=radioButton.getText().toString();
            if(typeMessageCat.equalsIgnoreCase("Phone")){
                messageType="0";

            }else{
                messageType="1";

            }


            String url= AppConfig.sendSingleSMSUrl("sendByNumber",business_id,phoneNumbers,content,messageType);
            httpRequest(url);
        }

    }


    private void httpRequest(String url){
        progressDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.v("RESPONSE",response.toString());
                parseData(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest.setTag("send_sms"));

    }
    private void parseData(JSONObject response){

        String messageSuccess;

        if(response != null && response.length() > 0){
            progressDialog.dismiss();
            try{
                if (response.getString("status").equalsIgnoreCase("success")){
                    messageSuccess=response.getString("message");
                    printMsg(messageSuccess);
                    etPhoneNumbers.setText("");
                    etMessage.setText("");
                }else{
                    String msg=response.getString("message");
                    printMsg(msg);

                }

            }catch (JSONException e){
                e.printStackTrace();
            }


        }

    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll("send_sms");
    }
}

