package karibusms.fragments;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import karibusms.R;
import karibusms.application.MyApplication;
import karibusms.database.SmsStore;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.logging.L;
import karibusms.services.ServiceResults;
import karibusms.services.karibubackground;
import me.tatarka.support.job.JobInfo;
import me.tatarka.support.job.JobScheduler;

public class SendSMS extends Fragment implements View.OnClickListener{
    private View view;
    private String owner_phone,business_id;
    private EditText sms;
    private Button sendsms;
    private String message;
    private Toolbar toolbar;

    //Run the JobSchedulerService every 2 minutes
    private static final long POLL_FREQUENCY = 288000;
    private JobScheduler mJobScheduler;
    private RequestQueue requestQueue;


    //int corresponding to the id of our JobSchedulerService
    private static final int JOB_ID = 100;
    private RadioButton radioTypeButton;
    private String category,msg_type;
    private RadioGroup msgtypeGroupId;
    private int msgoption;
    private Spinner spinner;
    private ArrayList<String> groups;
    private String b_group="all";
    private String url;
    private ErrorMessage errormsg;
    private TextView tv_text_info,tv_group_select,tv_kind_msg,tv_radio_bton_internet,tv_radio_btn_phone;
    private ProgressDialog progressDialog,loadGroupDialog;

    //String resultNumbers;

    SmsStore.KaribusmsdbHelper karibusmsdbHelper;


    public SendSMS() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_sendsms, container, false);
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Sending SMS...");
        progressDialog.setIndeterminate(false);

        loadGroupDialog=new ProgressDialog(getActivity());
        loadGroupDialog.setMessage("Loading...");
        loadGroupDialog.setIndeterminate(false);
        initializeSendSMSView();
        initializeSpinner();
        textFont();








        return view;


    }

    private void textFont(){
        tv_group_select=(TextView)view.findViewById(R.id.group_select);
        tv_kind_msg=(TextView)view.findViewById(R.id.kind_msg);
        tv_radio_btn_phone=(TextView)view.findViewById(R.id.radiobutton_phone);
        tv_radio_bton_internet=(TextView)view.findViewById(R.id.radiobutton_internet);
        tv_text_info=(TextView)view.findViewById(R.id.textinfo);

        String fontPath = "fonts/Lato-Regular.ttf";
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        tv_text_info.setTypeface(tf);
        tv_radio_bton_internet.setTypeface(tf);
        tv_radio_btn_phone.setTypeface(tf);
        tv_kind_msg.setTypeface(tf);
        tv_group_select.setTypeface(tf);


    }
    private void initializeSpinner(){
        groups= new ArrayList<>();
        groups.add("all");
        spinner=(Spinner)view.findViewById(R.id.group_spinner);
        spinner.setAdapter(new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                groups));
        spinner.setSelection(groups.indexOf("all"));
        String group_url=AppConfig.groupUrl("getGroup",business_id);
        httpRequestgroup(group_url);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View arg1, int position, long arg3) {
                b_group=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void onClick(View arg0) {

        karibusmsdbHelper = new SmsStore.KaribusmsdbHelper(getContext());

        message=sms.getText().toString();
        msgoption = msgtypeGroupId.getCheckedRadioButtonId();
        radioTypeButton = (RadioButton)view.findViewById(msgoption);

        //thread it

        if(message.length()==0){
           sms.setError("Message is required");
            return;
        }
        else if(msgoption<0){
           Toast.makeText(getActivity(),"Please select type of message",Toast.LENGTH_LONG).show();

            return;
        }
        else{
            category=(String)radioTypeButton.getText();
            if(category.equalsIgnoreCase("phone")){
                msg_type="0";

                // if RadioButton Phone isSelected then sends BulkSMS to Contacts category
                //using s new thread inCase of too many Contacts to Send SMS

                /*
                Runnable runnable= new Runnable() {
                    @Override
                    public void run() {
                        // Try fetching Contacts URL onLine
                        final String GET_CONTACTS_URL= AppConfig.getContacts("get_contacts",business_id);

                        final JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(GET_CONTACTS_URL, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                Log.v("MY DATA", response.toString());

                                // a for LOOP to get each phone number and send SMS to it.
                                String track;
                                for (int i = 0; i < response.length(); i++) {

                                    try {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Contact contact = new Contact();
                                        contact.setId(jsonObject.getString("subscriber_info_id"));

                                        // fetch jsonObject Phone_numbers only  put to track string
                                        track = jsonObject.getString("phone_number").toString();

                                        // sending message method
                                        SmsManager smsManager = SmsManager.getDefault();

                                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.SEND_SMS)
                                                != PackageManager.PERMISSION_GRANTED) {
                                            // requestNecessaryPermissions();
                                            Toast.makeText(getContext(), "No SMS Permission", Toast.LENGTH_SHORT).show();
                                        } else {
                                            // sending message method to each iterated phone number form string track
                                            smsManager.sendTextMessage(track, null, message, null, null);

                                            String status="Pending";
                                            //Send it to DataBase SmsStore
                                            karibusmsdbHelper.saveSms(track, message, status);

                                            //Toast.makeText(getContext(), "Message sent via Phone", Toast.LENGTH_SHORT).show();
                                        }

                                       // Toast.makeText(getContext(), "via Phone", Toast.LENGTH_SHORT).show();

                                        Log.i("Tracker", track);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }

                        });
                        VolleySingleton.getInstance().getRequestQueue().add(jsonArrayRequest);

                    }
                }; // End of the Runnable runnable created at top

                // create a thread that runs the runnable to avoid app crashing o long wait process
                Thread bulkSMSThread = new Thread(runnable);
                bulkSMSThread.start();
                */

            } else {
                msg_type="1";
            }
            url=AppConfig.sendsmsUrl("sendMessage",business_id,message,b_group,
                    msg_type);
            httpRequest(url);

        }
    }


    private void initializeSendSMSView(){
        requestQueue= Volley.newRequestQueue(getActivity());
        sendsms = (Button)view.findViewById(R.id.sendsms);
        sms = (EditText)view.findViewById(R.id.msg_content);
        owner_phone= Utils.readPreferences(getActivity(),"phonenumber","");
        business_id= Utils.readPreferences(getActivity(),"business_id","");
        msgtypeGroupId=(RadioGroup)view.findViewById(R.id.radio_type);
        sendsms.setOnClickListener(this);

    }


    private void  httpRequest(String url){
        progressDialog.show();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(req.setTag("send_sms"));
    }


    private void  httpRequestgroup(String url){
        loadGroupDialog.show();

        Log.d("Request",url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loadGroupDialog.dismiss();
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(req.setTag("send_sms"));
    }


    //This method will parse json data
    private void parseData(JSONObject response) {

        ArrayList<String>all_phonelist= new ArrayList<>();
        groups= new ArrayList<>();
        String msg;
        String link;
        if (response != null && response.length() > 0) {
            progressDialog.dismiss();
            try {
                if(response.getString("status").equalsIgnoreCase("success")) {
                    msg=response.getString("message");
                    link=response.getString("link");
                    JSONArray phonearray = response.getJSONArray("phone_post");
                    for (int i = 0; i < phonearray.length(); i++) {
                        JSONObject phones = phonearray.getJSONObject(i);
                        String phonenumber = phones.getString("phone_number");
                        all_phonelist.add(phonenumber);
                    }
                    MyApplication.getWritableDatabase().insertGroupMessage(all_phonelist, msg, link, true,
                            "karibusms");
                    getActivity().startService(new Intent(getActivity(),karibubackground.class));
                    setupJob();

                }else if (response.getString("status").equalsIgnoreCase("group")){
                    JSONArray grouparray = response.getJSONArray("groups");
                    for (int i = 0; i < grouparray.length(); i++) {
                        JSONObject phones = grouparray.getJSONObject(i);
                        String group = phones.getString("category");
                        groups.add(group);
                    }
                    groups.add("all");
                    spinner.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            groups));
                    spinner.setSelection(groups.indexOf("all"));

                }else{
                    printMsg(response.getString("message"));
                    sms.setText("");
                } }
            catch (JSONException e) {

            }
        }


    }

    private void setupJob() {
        mJobScheduler = JobScheduler.getInstance(getActivity());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //schedule the job after the delay has been elapsed
                buildJob();
            }
        }, 3000);
    }

    private void buildJob() {
        L.T(getActivity(),"Sending Messages please be patient");
        //attach the job ID and the name of the Service that will work in the background
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, new ComponentName(getActivity(), ServiceResults
                .class));
        //set periodic polling that needs net connection and works across device reboots
        builder.setPeriodic(POLL_FREQUENCY)
                .setPersisted(false);
        mJobScheduler.schedule(builder.build());
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll("send_sms");
    }
}
