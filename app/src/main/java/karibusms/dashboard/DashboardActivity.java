package karibusms.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import io.realm.Realm;
import karibusms.R;
import karibusms.account.LoginActivity;
import karibusms.adapters.DashboardViewAdapter;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.info.SMS;
import karibusms.services.SMSSendingService;


/**
 * Created by yohana on 7/5/16.
 */
public class DashboardActivity extends AppCompatActivity {

//    private DashboardRecyclerViewAdapter dashboardRecyclerViewAdapter;
//    private GridLayoutManager gridLayoutManager;
    private Intent intent;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private DashboardViewAdapter dashboardViewAdapter;
    private RecyclerView recyclerView;
    private SessionManager session;
    private Toolbar toolbar;
//    private List<Dashboard> items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);
        initializeView();
        setUpToolbar();
        initializeTabLayout();
        setupView();
        session=new SessionManager(getBaseContext());

        Realm realm = Realm.getDefaultInstance();
        long queued = realm.where(SMS.class).equalTo("status",SMS.PENDING_TO_CLIENT).count();
        long failed = realm.where(SMS.class).equalTo("status",SMS.FAILED_SENT).count();

        if (queued>0 || failed>0){
            startService(new Intent(this, SMSSendingService.class));
        }
    }

    public void initializeView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

    }


    public void initializeTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("HOME"));

            // new Update was required to remove About and Profile Tabs

        //tabLayout.addTab(tabLayout.newTab().setText("ABOUT"));
        //tabLayout.addTab(tabLayout.newTab().setText("PROFILE"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    public void setupView() {
        dashboardViewAdapter = new DashboardViewAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(dashboardViewAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }





    public  void setUpToolbar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("DASHBOARD");
    }


    public void infoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Please you have to Login first before you perform this activity");
        builder.setPositiveButton("GO TO LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item=menu.findItem(R.id.add_category);
        item.setVisible(false);

        if(!session.isLoggedIn()) {
            for (int i = 0; i < menu.size(); i++) {

                menu.getItem(0).setVisible(false);
            }
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.logout) {
            session.setLogin(false);
            Utils.deletePreferences(this);

            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
