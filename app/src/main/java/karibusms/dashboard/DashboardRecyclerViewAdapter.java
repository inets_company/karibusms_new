package karibusms.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import karibusms.R;


public class DashboardRecyclerViewAdapter extends RecyclerView.Adapter<DashboardRecyclerViewHolder> {
    private List<Dashboard> items;
    private Context context;

    public DashboardRecyclerViewAdapter(List<Dashboard> items, Context context) {
        this.items = items;
        this.context = context;
    }


    @Override
    public DashboardRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_card_view,null);
        DashboardRecyclerViewHolder rcv = new DashboardRecyclerViewHolder(view);
        return rcv;

    }

    @Override
    public void onBindViewHolder(DashboardRecyclerViewHolder holder, int position) {
        holder.dashboardName.setText(items.get(position).getDashboardName());
        holder.dashboardImage.setImageResource(items.get(position).getDashboardImage());

    }



    @Override
    public int getItemCount() {
        return items.size();
    }
}
