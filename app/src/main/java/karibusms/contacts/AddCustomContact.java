package karibusms.contacts;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import karibusms.R;
import karibusms.category.Category;
import karibusms.category.CategoryAdapter;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;



public class AddCustomContact extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{

    private  View view;
    private RequestQueue requestQueue;
    private List<String> allNumbers;
    private String businessname,business_id;
    private EditText etContactNumber,etContactEmail,etContactName;
    private TextView text_select,tv_text_info;
    private SessionManager session;
    private Spinner spinner;
    private ArrayList<String> groups;
    private String b_group="all";
    private ErrorMessage errormsg;
    private JSONArray contactArray;
    private ArrayList<Category> categoryList=new ArrayList<Category>();
    private ProgressDialog progressDialog;
    private String contactNumber,contactEmail,contactName;
    private Button sendContactBtn;
    private String category_id;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_add_custom_contact, container, false);
        sendContactBtn=(Button)view.findViewById(R.id.sendCustomContactsBtn);
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Adding Contact...");
        progressDialog.setIndeterminate(false);

        initializeUploadContactView();
        textViewsFont();
        return view;
    }

    private void initializeUploadContactView(){
        requestQueue=Volley.newRequestQueue(getActivity());
        contactArray =new JSONArray();
        groups=new ArrayList<>();
        groups.add("all");
        spinner=(Spinner)view.findViewById(R.id.group_choice);
        text_select=(TextView) view.findViewById(R.id.group_select);

        spinner.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                groups));

        spinner.setSelection(groups.indexOf("all"));
        spinner.setOnItemSelectedListener(this);
        sendContactBtn.setOnClickListener(this);

        etContactNumber = (EditText)view.findViewById(R.id.person_phone_number);
        etContactEmail=(EditText)view.findViewById(R.id.person_email);
        etContactName=(EditText)view.findViewById(R.id.person_name);

        tv_text_info=(TextView)view.findViewById(R.id.textinfo);
        businessname= Utils.readPreferences(getActivity(),"businessname","");
        business_id=Utils.readPreferences(getActivity(),"logged_in_id","");
        session=new SessionManager(getActivity());
        String group_url= AppConfig.groupUrl("getGroup",business_id);
        Log.v("GROUP URL",group_url.toString());
        httpRequest(group_url);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        //An item was selected. You can retrieve the selected item using
        //L.t(context,""+ parent.getItemAtPosition(pos));
        //b_group=parent.getItemAtPosition(pos).toString();
        // groups.add("all");
        b_group=parent.getItemAtPosition(pos).toString();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private void textViewsFont(){
        //Font path
        String fontPath = "fonts/Lato-Regular.ttf";
        //Loading Font Face
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        //Applying font
        text_select.setTypeface(tf);
        tv_text_info.setTypeface(tf);
    }


    @Override
    public void onClick(View v) {

        contactName=etContactName.getText().toString().trim();
        contactEmail=etContactEmail.getText().toString().trim();
        contactNumber=etContactNumber.getText().toString().trim();


        if(contactNumber.length() <10 || contactNumber.equalsIgnoreCase("")||
                contactEmail.equalsIgnoreCase("")||contactName.equalsIgnoreCase("")){
            Toast.makeText(v.getContext(),"All fields are required",Toast.LENGTH_LONG).show();
        }
        else{

            String urlAddContact=AppConfig.addCustomContactUrl("storeContacts",contactName,contactEmail,contactNumber,business_id,b_group);
            Log.v("STORE CONTACTS URL",urlAddContact.toString());
            httpRequest(urlAddContact);
        }

    }
    private void  httpRequest(String url){
        progressDialog.show();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(req.setTag(this));
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
    }
    //This method will parse json data
    private void parseData(JSONObject response) {

        etContactNumber.setText("");
        etContactEmail.setText("");
        etContactName.setText("");
        if (response != null && response.length() > 0) {

            try {
                if   (response.getString("status").equalsIgnoreCase("group")){
                    JSONArray grouparray = response.getJSONArray("groups");
                    // L.m("groups"+grouparray);
                    for (int i = 0; i < grouparray.length(); i++) {

                        JSONObject phones = grouparray.getJSONObject(i);
                        String categoryName = phones.getString("category");
                        String  categoryId=phones.getString("category_id");

                        groups.add(categoryName);

//                        Log.v("IDS",groups.toString());

                    }
//                    // groups.add("all");
                    spinner.setAdapter(new ArrayAdapter<>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            groups));

                    spinner.setVisibility(View.VISIBLE);
                    text_select.setVisibility(View.VISIBLE);

                    // spinner.setSelection(groups.indexOf("all"));

                }else {
                    printMsg(response.getString("message"));
                }
                // L.t(context,response.getString("message"));

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll(this);
    }
}
