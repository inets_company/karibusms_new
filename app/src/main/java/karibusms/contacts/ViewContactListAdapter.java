package karibusms.contacts;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import karibusms.R;


/**
 * Created by joshuajohn on 05/09/16.
 */
public class ViewContactListAdapter extends BaseAdapter {

    private List<Contact> contacts;
    private LayoutInflater inflater;
    private Activity activity;

    public ViewContactListAdapter(List<Contact> contacts,Activity activity) {
        this.contacts = contacts;
        this.activity=activity;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.view_contact_list_item, parent, false);

        TextView tv_contact_name=(TextView) convertView.findViewById(R.id.username);
        TextView tv_phone_number=(TextView) convertView.findViewById(R.id.contact_phone_number);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.contact_image_view);

        Contact c=contacts.get(position);
        tv_contact_name.setText(c.getContactName());
        tv_phone_number.setText(String.valueOf(c.getPhone_number()));

        char m =c.getContactName().charAt(0);
        String name=String.valueOf(m);


        ColorGenerator generator = ColorGenerator.MATERIAL;

        // generate random color
        int color = generator.getColor(getItem(position));
        //int color = generator.getRandomColor();

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(name, color); // radius in px

        imageView.setImageDrawable(drawable);
        return convertView;
    }
}
