package karibusms.contacts;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import karibusms.R;
import karibusms.activities.ContactsPickerActivity;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.logging.L;
import phonecontact.Contact;


/**
 * Created by INETS COMPANY LIMITED on 02/09/16.
 * Address Mikocheni B, Bima Road, Block no 11,Dar es salaam, Tanzania
 * +255 655 406 004 or +255 22 278 0228
 */
public class AddContact extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    private  View view;
    private RequestQueue requestQueue;
    private List<String> allNumbers;
    private String businessname,business_id;
    private EditText contactNumber,contactEmail,contactName;
    private TextView phonebook,text_select,tv_text_info;
    private String owner_phone;
    private Toolbar toolbar;
    private Button buttonPickContact;
    private ProgressBar progressBar;
    private String contact_numbers;
    private SessionManager session;
    private Spinner spinner;
    private ArrayList<String> groups;
    private String b_group="all";
    final int CONTACT_PICK_REQUEST = 1000;
    private static final int PERMISSION_REQUEST_CONTACT = 100;
    ErrorMessage errormsg;
    JSONObject contactInfo;
    JSONArray contactArray;
    private ProgressDialog addContactDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.add_contact_fragment, container, false);
        addContactDialog=new ProgressDialog(getActivity());
        addContactDialog.setMessage("Adding Contact...");
        addContactDialog.setIndeterminate(false);
        initializeUploadContactView();
        textViewsFont();

            // gives Contact read RunTime Permission to Application
        requestPermissions(
                new String[]{Manifest.permission.READ_CONTACTS},
                PERMISSION_REQUEST_CONTACT);

        return view;
    }


    private void initializeUploadContactView(){
        requestQueue= Volley.newRequestQueue(getActivity());
        contactArray = new JSONArray();
        groups=new ArrayList<>();
        groups.add("all");
        spinner=(Spinner)view.findViewById(R.id.group_choice);
        text_select=(TextView) view.findViewById(R.id.group_select);
        spinner.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                groups));
        spinner.setSelection(groups.indexOf("all"));
        spinner.setOnItemSelectedListener(this);
        progressBar = (ProgressBar)view.findViewById(R.id.uploadBar);
        buttonPickContact = (Button)view.findViewById(R.id.upload);

        contactNumber = (EditText)view.findViewById(R.id.phone_no);
        contactEmail=(EditText)view.findViewById(R.id.person_email);
        contactName=(EditText)view.findViewById(R.id.person_name);

        phonebook = (TextView)view.findViewById(R.id.phonebook);
        tv_text_info=(TextView)view.findViewById(R.id.textinfo);
        businessname= Utils.readPreferences(getActivity(),"businessname","");
        business_id=Utils.readPreferences(getActivity(),"logged_in_id","");
        phonebook.setOnClickListener(this);
        buttonPickContact.setOnClickListener(this);
        owner_phone=Utils.readPreferences(getActivity(),"phonenumber","");
        session=new SessionManager(getActivity());
        String group_url= AppConfig.groupUrl("getGroup",business_id);
        httpRequest(group_url);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        //L.t(context,""+ parent.getItemAtPosition(pos));
        b_group=parent.getItemAtPosition(pos).toString();
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private void textViewsFont(){
        // Font path
        String fontPath = "fonts/Lato-Regular.ttf";
        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        // Applying font
        text_select.setTypeface(tf);
        phonebook.setTypeface(tf);
        tv_text_info.setTypeface(tf);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.phonebook:
                Intent intentContactPick = new Intent(getActivity(),ContactsPickerActivity.class);
                startActivityForResult(intentContactPick,CONTACT_PICK_REQUEST);
                break;
            case R.id.upload:
                contact_numbers=contactNumber.getText().toString().trim();
                if(contact_numbers.equals("") || contact_numbers.length()<10){
                    contactNumber.setError(" Phone Number is required and must has 10 characters ");
                    return;
                 }

                List<String> contactList = Arrays.asList(contact_numbers.split(","));

                for (int i = 0; i < contactList.size(); i++) {
                    contactInfo=new JSONObject();
                    try {
                        contactInfo.put("phone_number", contactList.get(i));
                        contactInfo.put("name", "");
                        contactArray.put(i,contactInfo);
                    } catch (JSONException e) {

                    }
                }
                String url=AppConfig.uploadUrl("sync",contactArray.toString(),business_id,b_group);
                httpRequest(url);
                break;
            default:
                return;
        }
    }
    private void  httpRequest(String url){
        addContactDialog.show();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        addContactDialog.dismiss();

                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                addContactDialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(req.setTag(this));
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
    }
    //This method will parse json data
    private void parseData(JSONObject response) {

        contactNumber.setText("");
        if (response != null && response.length() > 0) {

            try {
                if   (response.getString("status").equalsIgnoreCase("group")){
                    JSONArray grouparray = response.getJSONArray("groups");
                    for (int i = 0; i < grouparray.length(); i++) {
                        JSONObject phones = grouparray.getJSONObject(i);
                        String group = phones.getString("category");
                        groups.add(group);
                    }

                    spinner.setAdapter(new ArrayAdapter<>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            groups));
                    spinner.setVisibility(View.VISIBLE);
                    text_select.setVisibility(View.VISIBLE);

                }else {
                    printMsg(response.getString("message"));
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONTACT_PICK_REQUEST && resultCode ==getActivity().RESULT_OK) {

            ArrayList<Contact> selectedContacts = data.getParcelableArrayListExtra("SelectedContacts");
            for(int i=0;i<selectedContacts.size();i++) {
                contactInfo=new JSONObject();
                try {
                    contactInfo.put("phone_number", selectedContacts.get(i).phone.toString().replaceAll("(?<=\\d) +(?=\\d)", ""));
                    contactInfo.put("name", selectedContacts.get(i).name.toString());
                    contactArray.put(i,contactInfo);
                } catch (JSONException e) {

                }
            }
            String url=AppConfig.uploadUrl("sync",contactArray.toString(),business_id,b_group);
            httpRequest(url);
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll(this);
    }
}
