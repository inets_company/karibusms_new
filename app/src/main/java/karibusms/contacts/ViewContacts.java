package karibusms.contacts;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;

import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import karibusms.R;
import karibusms.activities.Detail;
import karibusms.activities.SendingSMS;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;


public class ViewContacts extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private List<Contact> contactList=new ArrayList<Contact>();
    private ViewContactListAdapter viewContactListAdapter;
    private ListView contactListView;
    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog,deleteDialog;
    private ErrorMessage errormsg;
    private RequestQueue requestQueue;


    public ViewContacts() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        view= inflater.inflate(R.layout.view_contacts_fragment, container, false);

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        deleteDialog=new ProgressDialog(getActivity());
        deleteDialog.setMessage("Deleting Contact...");

        requestQueue= Volley.newRequestQueue(getActivity());

        contactListView = (ListView) view.findViewById(R.id.view_contact_list);
        viewContactListAdapter = new ViewContactListAdapter(contactList,getActivity());
        contactListView.setAdapter(viewContactListAdapter);
        registerForContextMenu(contactListView);

        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipe_to_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        getContactList();
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Contact");
        MenuInflater inflater=getActivity().getMenuInflater();
        inflater.inflate(R.menu.contact_context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position=info.position;
        Contact contact=(Contact)viewContactListAdapter.getItem(position);
        String subscriber_info_id=contact.getId();
        String business_id=Utils.readPreferences(getActivity(),"business_id","");
        String deleteContactUrl=AppConfig.deleteContactUrl("deleteContact",subscriber_info_id,business_id);

        Log.v("Subscriber Id",subscriber_info_id.toString());
        Log.v("Business Id",business_id.toString());
        Log.v("deleteContactUrl",deleteContactUrl.toString());

          switch (item.getItemId()){
              case R.id.delete_contact:
                  showDeleteDialog(deleteContactUrl);
                  deleteDialog.dismiss();
                  break;
          }
        return super.onContextItemSelected(item);
    }

    private void showDeleteDialog(final String url) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete Contact");
        builder.setMessage(" Delete this contact?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                deleteDialog.show();
                httpRequest(url);

            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    private void getContactList(){
        contactList.clear();
        progressDialog.show();
        String business_id= Utils.readPreferences(getActivity(),"business_id","");
        final String GET_CONTACTS_URL= AppConfig.getContacts("get_contacts",business_id);
        Log.v("MY URL",GET_CONTACTS_URL.toString());


        final JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(GET_CONTACTS_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.v("MY DATA", response.toString());


                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }


//                try {
//                    JSONObject jsonObject = response.getJSONObject(0);
//                    Contact contact = new Contact();
//                    contact.setId(jsonObject.getString("subscriber_info_id"));
//
//
//
//                }
//                catch (JSONException e) {
//                e.printStackTrace();
//                }


                StringBuffer sb = null;
                String wicker= "";
                String track= "";
                for (int i = 0; i < response.length(); i++) {

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        Contact contact = new Contact();
                        contact.setId(jsonObject.getString("subscriber_info_id"));

                        if (jsonObject.getString("phone_number").equalsIgnoreCase("null")
                                || jsonObject.getString("phone_number").isEmpty()) {
                            contact.setContactName("No phone Number");
                        } else {
                            contact.setPhone_number(jsonObject.getString("phone_number"));
                            //Toast.makeText(getContext(), " " + jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        }
                        if (jsonObject.getString("name").equalsIgnoreCase("null") ||
                                jsonObject.getString("name").isEmpty()) {
                            contact.setContactName("No Name");

                        } else {
                            contact.setContactName(jsonObject.getString("name"));
                        }

                        contactList.add(contact);


                        // trying my junk get 1 looong string

                        track = jsonObject.getString("phone_number").toString();
                        Log.i("Tracker", track);
                        wicker = wicker.concat(track);

                        //track.replaceAll("(.{3})", "$1,");
                        //sb = new StringBuffer();
                        //sb.append(track);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }



                StringBuilder str = new StringBuilder(wicker);

                int idx = str.length() - 12;
                while (idx > 0) {
                    str.insert(idx, ",");
                    idx = idx - 12;
                }
                String resultNumbers= String.valueOf(str);
                Log.i("Tracking", resultNumbers);

                viewContactListAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonArrayRequest);

    }


    @Override
    public void onRefresh() {
        contactList.clear();
        getContactList();
        swipeRefreshLayout.setRefreshing(false);
    }


    private void parseData(JSONObject response){
        L.m(response.toString());
        String messageSuccess;
        if(response != null && response.length() > 0){

            try{
                if (response.getString("status").equalsIgnoreCase("success")){
                    messageSuccess=response.getString("message");
                    printMsg(messageSuccess);
                    deleteDialog.dismiss();

                }else{
                    String msg=response.getString("message");
                    printMsg(msg);
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private void httpRequest(String url){
        deleteDialog.show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest.setTag(this));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll(this);
    }
}
