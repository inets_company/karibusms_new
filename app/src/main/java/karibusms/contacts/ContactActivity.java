package karibusms.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import karibusms.R;
import karibusms.dashboard.DashboardActivity;
import karibusms.extras.SessionManager;


public class ContactActivity extends AppCompatActivity {
    private Intent intent;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ContactViewAdapter contactViewAdapter;
    private SessionManager session;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_activity);
        initializeView();
        setUpToolbar();
        initializeTabLayout();
        setupView();
    }

    public void initializeView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

    }

    public  void setUpToolbar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void initializeTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("ADD CONTACT"));
        tabLayout.addTab(tabLayout.newTab().setText("ADD CUSTOM "));
        tabLayout.addTab(tabLayout.newTab().setText("VIEW CONTACT"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    public void setupView() {
        contactViewAdapter = new ContactViewAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(contactViewAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this,DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
