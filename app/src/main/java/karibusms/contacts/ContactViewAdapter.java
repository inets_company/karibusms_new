package karibusms.contacts;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by joshuajohn on 02/09/16.
 */
public class ContactViewAdapter extends FragmentPagerAdapter {
    int numTab;
    public ContactViewAdapter(FragmentManager fm,int numTab) {
        super(fm);
        this.numTab=numTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                AddContact add_contact=new AddContact();
                return add_contact;
            case 1:
                AddCustomContact addCustomContact=new AddCustomContact();
                return addCustomContact;
            case 2:
                ViewContacts view_contact=new ViewContacts();
                return view_contact;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return numTab;
    }
}
