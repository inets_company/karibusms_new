package karibusms.contacts;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.network.VolleySingleton;

public class ContactDetailActivity extends AppCompatActivity {
    private String name,subscriber_id,phone_number;
    private TextView tvName,tvPOhoneNumber;
    private ProgressDialog dialog;
    private ErrorMessage errorMessage;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        requestQueue= Volley.newRequestQueue(this);
        dialog=new ProgressDialog(this);
        dialog.setMessage("Deleting Contact..");
        dialog.setIndeterminate(false);
        setToolBar();

        Intent intent=getIntent();
        subscriber_id=intent.getStringExtra("subscriber_info_id");
        name=intent.getStringExtra("name");
        phone_number=intent.getStringExtra("phone_number");

        tvName=(TextView)findViewById(R.id.contact_name);
        tvName.setText("Name:" + name);
        tvPOhoneNumber=(TextView)findViewById(R.id.phone_number);
        tvPOhoneNumber.setText("Phone Number:" + phone_number);
    }

    private void setToolBar(){
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_contact_item:
                deleteContact();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteContact(){
        dialog.show();
        String id=subscriber_id;
        String business_id= Utils.readPreferences(ContactDetailActivity.this,"business_id","");
        String url= AppConfig.deleteContactUrl("deleteContact",id,business_id);
        Log.v("URL",url.toString());
        httpRequest(url);
    }

    private void httpRequest(String url){
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();

                Log.v("RESPONSE",response.toString());
                parseData(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();

                errorMessage.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest.setTag(this));

    }
    private void parseData(JSONObject response){
        String messageSuccess;

        if(response != null && response.length() > 0){
            try{
                if (response.getString("status").equalsIgnoreCase("success")){
                    messageSuccess=response.getString("message");
                    printMsg(messageSuccess);
                }else{
                    String msg=response.getString("message");
                    printMsg(msg);

                }

            }catch (JSONException e){
                e.printStackTrace();
            }


        }

    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("FeedBack Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll(this);
    }
}
