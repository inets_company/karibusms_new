package karibusms.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import karibusms.logging.L;

/**
 * Created by Yohana on 2/19/2016.
 */
public class FaqParser {
    public static ArrayList<String> parseFaqJSON(JSONObject response) {
        ArrayList<String>listFaq=new ArrayList<>();
        if (response != null && response.length() > 0) {
           // L.m("FAQRESPONSE"+response.toString());
            try {
                JSONArray arrayFaq = response.getJSONArray("faq");
                for (int i = 0; i < arrayFaq.length(); i++) {
                    JSONObject currentFaq = arrayFaq.getJSONObject(i);
                    String question=currentFaq.getString("question");
                    String answer=currentFaq.getString("answer");
                    if((question.trim()).length()==0 || (question.trim()).equalsIgnoreCase
                            ("null") || answer.trim() .isEmpty() || answer.equalsIgnoreCase("null")){

                    } else {
                        //Log.d("Inquiry Parser",response.toString());
                    listFaq.add(question);
                    listFaq.add(answer);
                    }
                }
            }catch (JSONException e){
                //Log.d("ARRAY LIST","ERRORS OCCURS");
            }
            //L.m(" array list"+ listFaq.toString());
           // Log.d("ARRAY LIST",listFaq.toString());
        }
        return listFaq;
    }
}
