package karibusms.adapters;


import android.app.Activity;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;


import java.util.List;
import karibusms.R;
import karibusms.info.SMSSent;



public class SentSMSAdapter extends BaseAdapter {
    private List<SMSSent> smsSents;
    private LayoutInflater inflater;
    private Activity activity;


    public SentSMSAdapter (Activity activity,  List<SMSSent> smsSents){
        this.activity=activity;
        this.smsSents=smsSents;

    }

    @Override
    public int getCount() {
        return smsSents.size();
    }

    @Override
    public Object getItem(int position) {
        return smsSents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.item_listview, parent, false);

        TextView message=(TextView)convertView.findViewById(R.id.message);

        ImageView imageView=(ImageView)convertView.findViewById(R.id.image_view);

        SMSSent s=smsSents.get(position);


        message.setText(s.getMessage());

        //get first letter of each String item)
       char m =s.getMessage().charAt(0);
        String msg=String.valueOf(m);
        

        ColorGenerator generator = ColorGenerator.MATERIAL;

        // generate random color
        int color = generator.getColor(getItem(position));
        //int color = generator.getRandomColor();

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(msg, color); // radius in px

        imageView.setImageDrawable(drawable);


        return convertView;
    }




}
