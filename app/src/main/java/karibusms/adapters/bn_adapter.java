package karibusms.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import karibusms.R;
import karibusms.info.Business;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;

/**
 * Created by Yohana on 4/26/2016.
 */
public class bn_adapter extends RecyclerView.Adapter<bn_adapter.ViewHolder> {
    //Imageloader to load image
    private VolleySingleton mVolleySingleton;
    Context context;
    private ImageLoader imageLoader;
    private static clickListener itemClick;
    private ArrayList<Business> mListBusiness = new ArrayList<>();
    private LayoutInflater mInflater;
    View v;
    String url="http://www.karibusms.com/media/images/business/profile_pic.png";


    public bn_adapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        mVolleySingleton = VolleySingleton.getInstance();
        imageLoader = mVolleySingleton.getImageLoader();
    }

    public void setbusiness(ArrayList<Business> businessinfo) {
        this.mListBusiness = businessinfo;
        //update the adapter to reflect the new set of movies
        notifyDataSetChanged();

    }
    public void animateTo(ArrayList<Business> businessinfo) {
        applyAndAnimateRemovals(businessinfo);
        applyAndAnimateAdditions(businessinfo);
        applyAndAnimateMovedItems(businessinfo);
    }

    public void applyAndAnimateRemovals(ArrayList<Business> newListBusiness) {
        for (int i = newListBusiness.size() - 1; i >= 0; i--) {
            final Business  business = newListBusiness.get(i);
            if (!mListBusiness.contains(business)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(ArrayList<Business> newListBusiness) {
        for (int i = 0, count = newListBusiness.size(); i < count; i++) {
            final Business  b = newListBusiness.get(i);
            if (!mListBusiness.contains(b)) {
                addItem(i, b);
            }
        }
    }

    private void applyAndAnimateMovedItems(ArrayList<Business> newListBusiness) {
        for (int toPosition = newListBusiness.size() - 1; toPosition >= 0; toPosition--) {
            final Business b = newListBusiness.get(toPosition);
            final int fromPosition = mListBusiness.indexOf(b);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Business removeItem(int position) {
        final Business  model = mListBusiness.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Business model) {
        mListBusiness.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Business model = mListBusiness.remove(fromPosition);
        mListBusiness.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         v = mInflater.inflate(R.layout.subscribe_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bname.setText(mListBusiness.get(position).getName());
        holder.blocation.setText(mListBusiness.get(position).getLocation());
        holder.business_id.setText(mListBusiness.get(position).getId());
        holder.business_phone.setText(mListBusiness.get(position).getPhone());
        holder.b_description.setText(mListBusiness.get(position).getDescription());
       // imageLoader.get(mListBusiness.get(position).getImageUrl(), ImageLoader.getImageListener(holder.p_imageview, R.drawable.gradient, android.R.color.black));
        // String urlthumbnail=mListBusiness.get(position).getImageUrl();
        String cover_urlthumbnail = mListBusiness.get(position).getImageUrl();
        loadProfile(url, holder);
     /*   if (!cover_urlthumbnail.isEmpty()) {
           // loadImages(cover_urlthumbnail, holder);
            imageLoader.get(mListBusiness.get(position).getImageUrl(), ImageLoader.getImageListener(holder.p_imageview, R.drawable.gradient, R.color.accent));
        } else {*/
            // holder.imageView.setImageUrl(urlthumbnail,imageLoader);
            if (position % 2 == 1) {
                if(mListBusiness.get(position).getImageUrl().equalsIgnoreCase("null")){
                    v.setBackgroundResource(R.drawable.gradient);

                } else

                imageLoader.get(mListBusiness.get(position).getImageUrl(), ImageLoader.getImageListener(holder.p_imageview, R.drawable.gradient, R.drawable.gradient_black));

            }
            else
            if(mListBusiness.get(position).getImageUrl().equalsIgnoreCase("null")){
                v.setBackgroundResource(R.drawable.gradient_black);

            } else


                imageLoader.get(mListBusiness.get(position).getImageUrl(), ImageLoader.getImageListener(holder.p_imageview, R.drawable.gradient_black, R.drawable.gradient));




        // v.setBackgroundResource(R.color.accent);
      // }
    }

    //use this function if you have used ImageView in your searchable layout file
    private void loadImages(String urlThumbnail, final ViewHolder holder) {
        if (!urlThumbnail.equals("")) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    //holder.imageView.setImageBitmap(response.getBitmap());
                    holder.p_imageview.setImageBitmap(response.getBitmap());
                }
                @Override
                public void onErrorResponse(VolleyError error) {
                   L.m( error.getMessage());
                }
            });
        }
    }

    //load the harcoded url circular profile_pic
    private void loadProfile(String urlThumbnail, final ViewHolder holder) {
        if (!urlThumbnail.equals("")) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.imageView.setImageBitmap(response.getBitmap());
                    //holder.p_imageview.setImageBitmap(response.getBitmap());
                }
                @Override
                public void onErrorResponse(VolleyError error) {
                    L.t(context,"Image failed to load");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mListBusiness.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //Views
        public CircleImageView imageView;
        public ImageView p_imageview;
        public TextView bname;
        public TextView business_phone;
        public TextView bsubscriber;
        public TextView blocation;
        public TextView business_id;
        public  TextView b_description;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (CircleImageView) itemView.findViewById(R.id.profile_image);
            p_imageview = (ImageView) itemView.findViewById(R.id.cover_pic);
            bname = (TextView) itemView.findViewById(R.id.b_name);
            business_id=(TextView) itemView.findViewById(R.id.business_id);
            blocation=(TextView) itemView.findViewById(R.id.b_location);
            business_phone=(TextView) itemView.findViewById(R.id.phonenumber);
            b_description=(TextView) itemView.findViewById(R.id.more_description);
            // We set listeners to the whole item view, but we could also
            // specify listeners for the imageView or the photoName.
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            //imageView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClick != null) {
                itemClick.onItemClicked(getLayoutPosition());
            } else {
                Toast.makeText(context, "ItemClick listeners is null", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /* Setter for listener.*/
    public void setItemClick(clickListener itemClick) {
        this.itemClick = itemClick;
    }

    public interface clickListener {

        void onItemClicked(int position);
    }

    public clickListener getItemClick() {
        return itemClick;
    }


}
