package karibusms.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import karibusms.fragments.About;
import karibusms.fragments.Home;
import karibusms.fragments.Profile;


/**
 * Created by joshuajohn on 02/09/16.
 */
public class DashboardViewAdapter extends FragmentPagerAdapter {
    int numTab;

    public DashboardViewAdapter(FragmentManager fm ,int numTab) {
        super(fm);
        this.numTab=numTab;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                Home home = new Home();
                return home;
            case 1:
                About about = new About();
                return about;
            case 2:
                Profile profile = new Profile();
                return profile;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return numTab;
    }
}
