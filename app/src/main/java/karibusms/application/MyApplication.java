package karibusms.application;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import karibusms.database.SmsStore;
import karibusms.extras.Utils;
import karibusms.logging.L;

/**
 * Created by Yohana on 2/19/2016.
 */
public class MyApplication extends Application {
    public static final String API_KEY = "107xwsfgate93iojdnmdrvyu";
    private static MyApplication sInstance;
    public static String lang;
    private static SmsStore db;

    public static MyApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }


    public synchronized static SmsStore getWritableDatabase() {
        if (db == null) {
            db = new SmsStore(getAppContext());
        }
        return db;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        db = new SmsStore(this);
        Realm.init(this);
    }
    public static String language() {
        lang= Utils.readPreferences(getAppContext(),"lang","");
return lang;
    }

}
