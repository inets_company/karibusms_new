package karibusms.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import karibusms.R;
import karibusms.dashboard.DashboardActivity;
import karibusms.extras.AppConfig;
import karibusms.extras.Utils;
import karibusms.info.SMS;


public class StatisticActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView pending_sms_number,sent_sms_number,number_of_contacts,remain_sms_number;
    private TextView pending,sent_sms,people,remain_sms;
    private RequestQueue requestQueue;
    private String txt_pending="0";
    private String no_contacts="0";
    private String txt_sms_remain="0";
    private String txt_sms_sent= "0";
    private ProgressDialog progressDialog;
    private Runnable mLocalStatsRunnable;
    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistic_activity);

        pending=(TextView)findViewById(R.id.txt_panding);
        sent_sms=(TextView)findViewById(R.id.sent_sms_title);
        people=(TextView)findViewById(R.id.text_people);
        remain_sms=(TextView)findViewById(R.id.text_remain_sms);
        pending_sms_number=(TextView)findViewById(R.id.txt_pending_number);
        sent_sms_number=(TextView)findViewById(R.id.sent_sms_number);
        remain_sms_number=(TextView)findViewById(R.id.text_remain_number);
        number_of_contacts=(TextView)findViewById(R.id.text_people_number);

        mHandler = new Handler();
        mLocalStatsRunnable = new Runnable() {
            @Override
            public void run() {
                showLocalStats();
            }
        };

        showLocalStats();

        progressDialog=new ProgressDialog(StatisticActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.show();
        setToolBar();
        textFont();
        getStatistics();
    }

    private void showLocalStats() {
        Realm realm = Realm.getDefaultInstance();
        long queued = realm.where(SMS.class).equalTo("status",SMS.PENDING_TO_CLIENT).count();
        long failed = realm.where(SMS.class).equalTo("status",SMS.FAILED_SENT).count();
        long sending = realm.where(SMS.class).equalTo("status",SMS.SENDING).count();
        realm.close();
        ((TextView)findViewById(R.id.txt_queued_number)).setText(String.valueOf(queued));
        ((TextView)findViewById(R.id.txt_queued_failed)).setText(String.valueOf(failed));
        ((TextView)findViewById(R.id.txt_sending)).setText(String.valueOf(sending));

        mHandler.postDelayed(mLocalStatsRunnable,10000);
    }

    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void textFont(){


        String fontPath="fonts/Lato-Regular.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        pending.setTypeface(tf);
        sent_sms.setTypeface(tf);
        people.setTypeface(tf);
        number_of_contacts.setTypeface(tf);
        remain_sms_number.setTypeface(tf);
        remain_sms.setTypeface(tf);
        pending_sms_number.setTypeface(tf);
        sent_sms_number.setTypeface(tf);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
      }

    private void getStatistics(){
        String business_id= Utils.readPreferences(StatisticActivity.this,"business_id","");
        String GET_STATISTICS_URL= AppConfig.statisticUrl("statistics",business_id);

         //Log.d("STATISTICS",GET_STATISTICS_URL);

        requestQueue=Volley.newRequestQueue(StatisticActivity.this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(GET_STATISTICS_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                for(int i=0; i<response.length(); i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        if(object.getString("sentsms").equalsIgnoreCase("null")){
                            pending_sms_number.setText("0");
                        }
                        else{
                            txt_sms_sent=object.getString("sentsms");
                        }

                        if(object.getString("subscribers").equalsIgnoreCase("null")){
                            number_of_contacts.setText("0");
                        }
                        else{
                            no_contacts=object.getString("subscribers");
                        }

                        if(object.getString("pendingsms").equalsIgnoreCase("null")){
                            pending_sms_number.setText("0");
                        }
                        else{
                            txt_pending=object.getString("pendingsms");
                        }

                        if(object.getString("remainingsms").equalsIgnoreCase("null")){
                            remain_sms_number.setText("0");
                        }
                        else{
                            txt_sms_remain=object.getString("remainingsms");
                        }


                    } catch (JSONException e){
                        e.printStackTrace();
                    }

                    number_of_contacts.setText(no_contacts);
                    remain_sms_number.setText(txt_sms_remain);
                    pending_sms_number.setText(txt_pending);
                    sent_sms_number.setText(txt_sms_sent);

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

            }
        });
        requestQueue.add(jsonArrayRequest.setTag("stats"));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requestQueue!=null)
            requestQueue.cancelAll("stats");

        if (mHandler!=null)
            mHandler.removeCallbacks(mLocalStatsRunnable);
    }
}

