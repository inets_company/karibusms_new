package karibusms.activities;


import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import karibusms.R;
import karibusms.application.MyApplication;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.logging.L;
import karibusms.services.ServiceResults;
import karibusms.services.karibubackground;
import me.tatarka.support.job.JobInfo;
import me.tatarka.support.job.JobScheduler;

public class SendSms extends AppCompatActivity implements View.OnClickListener {
    //Run the JobSchedulerService every 2 minutes
    private static final long POLL_FREQUENCY = 288000;
    //int corresponding to the id of our JobSchedulerService
    private static final int JOB_ID = 100;
    TextView text_select;
    private String owner_phone, business_id;
    private EditText sms;
    private Button sendsms;
    private String message;
    private Toolbar toolbar;
    private JobScheduler mJobScheduler;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private RadioButton radioTypeButton;
    private String category, msg_type;
    private RadioGroup msgtypeGroupId;
    private int msgoption;
    private Spinner spinner;
    private ArrayList<String> groups;
    private String b_group = "all";
    private String url;
    private ErrorMessage errormsg;
    private Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.send_message);
        setUpSMSToolbar();
        initializeSendSMSView();
        initializeSpinner();


    }

    @SuppressWarnings("ConstantConditions")
    private void setUpSMSToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initializeSpinner() {
        groups = new ArrayList<>();
        groups.add("all");

        text_select = (TextView) findViewById(R.id.group_select);
        spinner = (Spinner) findViewById(R.id.group_spinner);
        spinner.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                groups));
        spinner.setSelection(groups.indexOf("all"));
        String group_url = AppConfig.groupUrl("getGroup", business_id);
        //upload contacts
        httpRequestgroup(group_url);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                b_group = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        message = sms.getText().toString();
        msgoption = msgtypeGroupId.getCheckedRadioButtonId();
        radioTypeButton = (RadioButton) findViewById(msgoption);
        if (message.length() == 0) {
            L.t(this, "Please enter the message first");
            return;
        } else if (msgoption < 0) {
            // Snackbar.make(,"Please select type of messaging" , Snackbar.LENGTH_LONG).show();
            Toast.makeText(this, "Please select type of Message", Toast.LENGTH_LONG).show();
            return;

        } else {
            category = (String) radioTypeButton.getText();
            if (category.equalsIgnoreCase("phone")) {
                msg_type = "0";
            } else {
                msg_type = "1";
            }
            url = AppConfig.sendsmsUrl("sendMessage", business_id, message, b_group,
                    msg_type);
            httpRequest(url);

        }
    }

    private void initializeSendSMSView() {
        requestQueue = Volley.newRequestQueue(this);
        progressBar = (ProgressBar) findViewById(R.id.uploadBar);
        sendsms = (Button) findViewById(R.id.sendsms);
        sms = (EditText) findViewById(R.id.msg_content);
        owner_phone = Utils.readPreferences(this, "phonenumber", "");
        business_id = Utils.readPreferences(this, "logged_in_id", "");
        msgtypeGroupId = (RadioGroup) findViewById(R.id.radio_type);
        sendsms.setOnClickListener(this);

    }


    private void httpRequest(String url) {
    // Post params to be sent to the server
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);
                errormsg = new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });
        // add the request object to the queue to be executed
        requestQueue.add(req);
    }


    private void httpRequestgroup(String url) {
// Post params to be sent to the server
        progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                errormsg = new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });
// add the request object to the queue to be executed
        requestQueue.add(req);
    }

    //This method will parse json data
    private void parseData(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        L.m("From server data" + response.toString());
        sms.setText("");
        ArrayList<String> all_phonelist = new ArrayList<String>();
        groups = new ArrayList<>();
        String msg;
        String link;
        if (response != null && response.length() > 0) {
            try {
                if (response.getString("status").equalsIgnoreCase("success")) {
                    // L.t(getActivity(), "The system is sending message");
                    msg = response.getString("message");
                    link = response.getString("link");
                    JSONArray phonearray = response.getJSONArray("phone_post");
                    for (int i = 0; i < phonearray.length(); i++) {
                        JSONObject phones = phonearray.getJSONObject(i);
                        String phonenumber = phones.getString("phone_number");
                        all_phonelist.add(phonenumber);
                    }
                    MyApplication.getWritableDatabase().insertGroupMessage(all_phonelist, msg, link, true,
                            "karibusms");
                    startService(new Intent(this, karibubackground.class));
                    setupJob();
                } else if (response.getString("status").equalsIgnoreCase("group")) {
                    JSONArray grouparray = response.getJSONArray("groups");
                    for (int i = 0; i < grouparray.length(); i++) {
                        JSONObject phones = grouparray.getJSONObject(i);
                        String group = phones.getString("group");
                        groups.add(group);
                    }

                    spinner.setVisibility(View.VISIBLE);
                    text_select.setVisibility(View.VISIBLE);
                    groups.add("all");
                    spinner.setAdapter(new ArrayAdapter<>(this,
                            android.R.layout.simple_spinner_dropdown_item,
                            groups));
                    spinner.setSelection(groups.indexOf("all"));

                } else {
                    printMsg(response.getString("message"));
                }
            } catch (JSONException e) {

            }
        }


    }

    private void setupJob() {
        mJobScheduler = JobScheduler.getInstance(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //schedule the job after the delay has been elapsed
                buildJob();
            }
        }, 3000);
    }

    private void buildJob() {
        L.T(this, "Sending Messages please be patient");
        //attach the job ID and the name of the Service that will work in the background
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, new ComponentName(this, ServiceResults
                .class));
        //set periodic polling that needs net connection and works across device reboots
        builder.setPeriodic(POLL_FREQUENCY)
                .setPersisted(false);
        mJobScheduler.schedule(builder.build());
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
    }


}

