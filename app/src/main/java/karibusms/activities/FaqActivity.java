package karibusms.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import karibusms.R;
import karibusms.adapters.ExpandableListAdaptor;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.json.FaqParser;
import karibusms.logging.L;

public class FaqActivity extends AppCompatActivity  {
    ExpandableListAdaptor listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private Context contex = this;
    ErrorMessage errormsg;
    private String faqurl;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    Intent intent;
    Menu Mymenu;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_faq);
       // setUpToolbar();
        expListView = (ExpandableListView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar. setBackgroundDrawable(new ColorDrawable(Color.parseColor("#53b567")));
        requestQueue = Volley.newRequestQueue(this);
        SendJSONRequest();

    }
    private void SendJSONRequest(){
        progressBar.setVisibility(View.VISIBLE);
        // Do animation start
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, AppConfig.faqUrl("faq"),null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//L.m(response.toString());
                        updateDisplay( FaqParser.parseFaqJSON(response));
                        }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleVolleyError(error);
            }
        });
        requestQueue.add(req);
    }
    public void handleVolleyError(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        errormsg=new ErrorMessage(contex);
        errormsg. handleVolleyError(error);
    }
    private  void updateDisplay(List<String>ListData) {
        progressBar.setVisibility(View.GONE);
        resetUpdating();
        L.m("FAQ LIST" + ListData.toString());
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap <>();
        //resetUpdating();
        if (!ListData.isEmpty() || ListData.size() > 0) {
            //MyApplication.getWritableDatabase().insertFaq(faqlist, true, "faq");
            L.m("FAQ LIST" + ListData.toString());
            //Intent intent = getIntent();
            //finish();
            // startActivity(intent);
            for (int i = 0; i < ListData.size(); i += 2) {
                List<String> headercontents = new ArrayList<String>();
                String qn = ListData.get(i);
                String ans = ListData.get(i + 1);
                headercontents.add(ans);
                // headercontents.add(listDataChild.g)
                listDataHeader.add(qn);
                listDataChild.put(qn, headercontents);
            }
            L.m("LIST OF data" + " " + listDataChild);
            listAdapter = new ExpandableListAdaptor(contex, listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            // Listview Category click listener
            expListView.setOnGroupClickListener(new OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    // Toast.makeText(getApplicationContext(),
                    // "Category Clicked " + listDataHeader.get(groupPosition),
                    // Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            // Listview Category expanded listener
            expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                       /* Toast.makeText(getApplicationContext(),
                                listDataHeader.get(groupPosition) + " Expanded",
                                Toast.LENGTH_SHORT).show();*/
                }
            });

            // Listview Category collasped listener
            expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {


                }
            });

            // Listview on child click listener
            expListView.setOnChildClickListener(new OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    // TODO Auto-generated method stub

                    return false;
                }
            });
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.refresh:
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                ImageView iv = (ImageView)inflater.inflate(R.layout.iv_refresh, null);
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
                rotation.setRepeatCount(Animation.INFINITE);
                iv.startAnimation(rotation);
                item.setActionView(iv);
                SendJSONRequest();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        getMenuInflater().inflate(R.menu.refresh, menu);

        Mymenu = menu;
   /*     for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            spanString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spanString.length(), 0); //fix the color to white
            item.setTitle(spanString);
        }*/

        return true;
    }
    public void resetUpdating()
    {
        // Get our refresh item from the menu
        MenuItem m = Mymenu.findItem(R.id.refresh);
        if(m.getActionView()!=null)
        {
            // Remove the animation.
            m.getActionView().clearAnimation();
            m.setActionView(null);
        }
    }


}
