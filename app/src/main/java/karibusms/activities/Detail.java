package karibusms.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.logging.L;


public class Detail extends AppCompatActivity implements View.OnClickListener {
    public  TextView tv_msg,tv_no_people,tv_category,tv_time;
    private String msg;
    private String people;
    private String category;
    private String smsId;
    private String time;
    private ErrorMessage errormsg;
    private Toolbar toolbar;
    private RequestQueue requestQueue;
    private Button deleteMessageButton;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sent_message_datail);
        requestQueue= Volley.newRequestQueue(this);
        dialog=new ProgressDialog(this);
        dialog.setMessage("Deleting SMS");
        dialog.setIndeterminate(true);
        deleteMessageButton=(Button)findViewById(R.id.deleteBtn);
        deleteMessageButton.setOnClickListener(this);
        setToolBar();
        Intent intent=getIntent();
        if(intent!=null){
            smsId=intent.getStringExtra("id");
            msg=intent.getStringExtra("message");
            people=intent.getStringExtra("people");
            category=intent.getStringExtra("category");
            time=intent.getStringExtra("time");
        }

        tv_msg=(TextView)findViewById(R.id.message);
        tv_msg.setText(msg);

        tv_no_people=(TextView)findViewById(R.id.no_people);
        tv_no_people.setText(people);

        tv_category=(TextView)findViewById(R.id.category);
        tv_category.setText(category.toUpperCase());

        tv_time=(TextView)findViewById(R.id.time_sent);
        tv_time.setText(time);
    }
    private void setToolBar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        dialog.show();
        String message_id=smsId;
        String business_id= Utils.readPreferences(Detail.this,"business_id","");
        final String deleteUrl= AppConfig.sendDeleteMessageUrl("deleteMessage",business_id,message_id);
        httpRequest(deleteUrl);
    }

    private void parseData(JSONObject response){
        dialog.show();
        L.m(response.toString());
        String messageSuccess;
        if(response != null && response.length() > 0){
            dialog.dismiss();
            try{
                if (response.getString("status").equalsIgnoreCase("success")){
                    messageSuccess=response.getString("message");
                    printMsg(messageSuccess);


                }else{
                    String msg=response.getString("message");
                    printMsg(msg);
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private void httpRequest(String url){
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                errormsg=new ErrorMessage(Detail.this);
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
