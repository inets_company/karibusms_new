package karibusms.info;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Memory Mwageni on 22/11/2017, For SMSGateway project.
 */

public class PendingRequest extends RealmObject {

    @Ignore public final static int TYPE_CHECK_NEW_SMS_TO_SEND=0x1;
    @Ignore public final static int TYPE_SEND_REPORT=0x2;

    @Ignore public final static int STATUS_PENDING=0x0;
    @Ignore public final static int STATUS_PROCESSED=0x1;

    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private int type;
    private long time;
    private int status;

    public PendingRequest() {}

    public PendingRequest(int type, long time,int status) {
        this.type = type;
        this.time = time;
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
