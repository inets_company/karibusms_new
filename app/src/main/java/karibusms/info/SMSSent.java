package karibusms.info;

import java.io.Serializable;

/**
 * Created by joshuajohn on 25/08/16.
 */
public class SMSSent implements Serializable {

    private String message;
    private String status;
    private String noPeople;
    private String category;
    private String time;
    private String smsId;
    private String smstyp;

    public String getSmsId() {
        return smsId;
    }

    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(String noPeople) {
        this.noPeople = noPeople;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getSmstyp() {
        return smstyp;
    }

    public void setSmstyp(String smstyp) {
        this.smstyp = smstyp;
    }

}
