package karibusms.info;

/**
 * Created by joshuajohn on 15/09/16.
 */
public class Statistics {

    private String smsSent;
    private String pendingSMS;
    private String numberOfPeople;
    private String remainingSMS;

    public Statistics() {
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public String getPendingSMS() {
        return pendingSMS;
    }

    public void setPendingSMS(String pendingSMS) {
        this.pendingSMS = pendingSMS;
    }

    public String getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public String getRemainingSMS() {
        return remainingSMS;
    }

    public void setRemainingSMS(String remainingSMS) {
        this.remainingSMS = remainingSMS;
    }
}
