package karibusms.info;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Memory Mwageni on 11/09/2017, For SMSGateway project.
 */

public class SMS extends RealmObject {

    /**
     * Pending to be sent to the phone number
     */
    @Ignore public static final int PENDING_TO_CLIENT=1;
    @Ignore public static final int SENT=2;
    @Ignore public static final int FAILED_SENT=3;
    @Ignore public static final int DELIVERED=4;
    @Ignore public static final int FAILED_DELIVERY =5;
    /**
     * Currently being sent
     */
    @Ignore public static final int SENDING=6;

    /**
     * Received message which have been processed, Sent to server.
     */
    @Ignore public static final int PROCESSED=7;
    /**
     * Received, Waiting to be sent to the server
     */
    @Ignore public static final int PENDING_TO_SERVER=8;

    @Ignore public static final int SENT_SERVER_KNOWS=9;
    @Ignore public static final int FAILED_AFTER_MAX_RETRIES = 3;

    @PrimaryKey
    private String id = UUID.randomUUID().toString();

    private String phoneNumber;
    private String body;
    private long time;
    private int status;
    private int retriesCount = -1;
    private long timeSent;

    public SMS() {
    }

    public SMS(String phoneNumber, String body, long time, int status) {
        this.phoneNumber = phoneNumber;
        this.body = body;
        this.time = time;
        this.status = status;
    }

    public String getPhoneNumber() {
        return "+"+phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRetriesCount() {
        return retriesCount;
    }

    public void setRetriesCount(int retriesCount) {
        this.retriesCount = retriesCount;
    }

    public void setTimeSent(long timeSent) {
        this.timeSent = timeSent;
    }

    public long getTimeSent() {
        return timeSent;
    }
}
