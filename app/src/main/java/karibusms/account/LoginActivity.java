package karibusms.account;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import karibusms.R;

import karibusms.dashboard.DashboardActivity;;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.logging.L;
import karibusms.services.GCMRegistrationIntentService;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{


    private TextView registerHere;
    private Button signIn;
    private Toolbar toolbar;
    private TextInputLayout phoneLogin;
    private TextInputLayout passwordLogin;
    private EditText etPhoneLogin;
    private EditText etPasswordLogin;
    private RequestQueue requestQueue;
    private SessionManager session;
    private ProgressBar progressBar;
    private Context context=this;
    private ErrorMessage errormsg;
    String gcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.karibu_login);
        initializeRegisterViews();
        gcmCall();
        setUpToolbar();
    }

    public  void setUpToolbar() {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
         }

    private void gcmCall(){
        gcm_id=Utils.readPreferences(this,"token","");
        if(gcm_id.isEmpty()){
            this.startService(new Intent(this,GCMRegistrationIntentService.class));
        }
    }


    private void initializeRegisterViews(){

        requestQueue = Volley.newRequestQueue(this);
        registerHere=(TextView)findViewById(R.id.registerhere);
        registerHere.setLinksClickable(true);
        signIn=(Button)findViewById(R.id.signin_button);
        phoneLogin=(TextInputLayout)findViewById(R.id.phone_loginlayout);
        passwordLogin=(TextInputLayout)findViewById(R.id.password_loginlayout);
        etPhoneLogin = (EditText) findViewById(R.id.phone_login);
        etPasswordLogin = (EditText) findViewById(R.id.password_login);
        progressBar = (ProgressBar)findViewById(R.id.uploadBar);
        registerHere.setOnClickListener(this);
        signIn.setOnClickListener(this);
        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }

    }

    /**
     * function to verify login details
     * */
    private void checkLogin(String url) {
        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url ,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }});
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        L.m(response.toString());
        String message;
        String status;
        String profile_link;
        if (response != null && response.length() > 0) {
            try {
                status=response.getString("status");
                if(status.equalsIgnoreCase("success")){
                    session=new SessionManager(context);
                    String business_id=response.getString("business_id");
                    String b_name=response.getString("business_name");
                    profile_link=response.getString("profile_link");
                    String phonenumber=response.getString("phone_number");
                    Utils.savePreferences(LoginActivity.this, "business_id", business_id);
                    Utils.savePreferences(LoginActivity.this, "logged_in_id", business_id);
                    Utils.savePreferences(LoginActivity.this, "business_owner",b_name);
                    Utils.savePreferences(LoginActivity.this, "owner_number", phonenumber);
                    Utils.savePreferences(LoginActivity.this, "profile_link",profile_link);
                    session.setLogin(true);
                    //Launch login activity
                     Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                     startActivity(intent);
                     finish();
                }
                else {
                     message=response.getString("message");
                    L.t(context, message);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            //on clicking register button move to Register Activity
            case R.id.registerhere:
                Intent intent = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(intent);
                finish();
                break;

            //on clicking the signin button check for the empty field then call the checkLogin() function
            case R.id.signin_button:
                gcmCall();
                String phonenumber = etPhoneLogin.getText().toString().trim();
                String password = etPasswordLogin.getText().toString();

                if (TextUtils.isEmpty(gcm_id)){
                    Snackbar.make(v, "GCM not initialized!", Snackbar.LENGTH_LONG)
                            .show();
                    return;
                }

                // Check for empty data
                if (phonenumber.trim().length() > 0 && password.trim().length() > 0) {
                    // login user
                    String url=AppConfig.loginUrl("login",phonenumber,password,gcm_id,Utils
                            .deviceMan,Utils.deviceName);
                    L.m("TEST GCM"+gcm_id);
                    L.m(url);
                    checkLogin(url);
                }
                else {
                    // show snackbar to enter credentials
                    Snackbar.make(v, "Please enter the credentials!", Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void backHome(View view){
        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }
    public void forgotPassword(View view){
        String url = "http://karibusms.com/password/email";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


}
