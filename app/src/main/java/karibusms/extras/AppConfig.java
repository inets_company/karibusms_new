package karibusms.extras;
import android.net.Uri;



public class AppConfig {

	public static String TEST_URL = "http://karibusms.com/kiosk/claim.php";
	private static String AUTHORITY3 = "192.168.43.75:80";
	private static String AUTHORITYy = "karibusms.com";
	private static String AUTHORITYx = "10.0.2.2";
	private static String AUTHORITY = "51.77.212.234:8282";

	public static String SERVER_URL = "http://"+AUTHORITY+"/android_test?";

	public static String profile(String tag,String page) {
		return SERVER_URL+"tag="+tag+"&page="+page;
	}

	public static String loginUrl(String tag,String phonenumber,String password,String gcm_id,
								  String manufacturer,String model){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("phone_number", phonenumber)
				.appendQueryParameter("password", password)
				.appendQueryParameter("gcm_id", gcm_id)
				.appendQueryParameter("manufacturer", manufacturer)
				.appendQueryParameter("model", model);
		return builder.build().toString();
	}
	public static String registerUrl(String tag,String phonenumber,String password,String
			business_name,String gcm_id,String email,String business_type){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("phone_number", phonenumber)
				.appendQueryParameter("password", password)
				.appendQueryParameter("business_name", business_name)
				.appendQueryParameter("gcm_id", gcm_id)
				.appendQueryParameter("email", email)
				.appendQueryParameter("business_type", business_type);
		return builder.build().toString();
	}
	public static String subscribeUrl(String tag,String phonenumber,String phoneime,String
			business_id){
		String subscribe_url=SERVER_URL+"tag="+tag+"&sub_phone="+phonenumber+"&phoneime" +
				"="+phoneime+"&business_id="+business_id;
		return subscribe_url;
	}
	public static String contactBusiness(String tag,String phonenumber,String message,String
			business_id){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .encodedAuthority(AUTHORITY)
                .appendPath("android_test")
                .appendQueryParameter("tag", tag)
                .appendQueryParameter("sub_phone", phonenumber)
                .appendQueryParameter("message", message)
                .appendQueryParameter("business_id", business_id);
		return builder.build().toString();
	}
	public static String uploadUrl(String tag,String contact,String
			business_id,String category_name){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("contacts", contact)
				.appendQueryParameter("business_id", business_id)
				.appendQueryParameter("category_name", category_name);
		return builder.build().toString();
	}
	public static String sendsmsUrl(String tag,String business_id,String content,String
			category_name,String message_type){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("business_id", business_id)
				.appendQueryParameter("content", content)
				.appendQueryParameter("category_name", category_name)
				.appendQueryParameter("message_type", message_type);
		return builder.build().toString();
	}
	public static String sendSingleSMSUrl(String tag,String business_id,String phone_number,String content,String message_type)
	{
		 Uri.Builder builder=new Uri.Builder();
		 builder.scheme("http")
				 .encodedAuthority(AUTHORITY)
				 .appendPath("android_test")
				 .appendQueryParameter("tag", tag)
				 .appendQueryParameter("business_id", business_id)
                 .appendQueryParameter("phone_numbers",phone_number)
				 .appendQueryParameter("content", content)
				 .appendQueryParameter("message_type", message_type);
		return builder.build().toString();

	}

	public static String pullSMS(String business_id) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", "pullMessagesToSend")
				.appendQueryParameter("business_id", business_id);
        return builder.build().toString();
	}

	public static String sendFeedbackSMS(String phoneNumber,String content,String business_id){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", "storeIncomingMessage")
				.appendQueryParameter("phone_number", phoneNumber)
				.appendQueryParameter("content", content)
				.appendQueryParameter("business_id", business_id);
        return builder.build().toString();
	}

	public static String sendSMSReports(String business_id,String data){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", "updateMessageSendingStatus")
				.appendQueryParameter("messages", data)
				.appendQueryParameter("business_id", business_id);
		return builder.build().toString();
	}

	public static String sayHello(String business_id) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", "helloFromApp")
				.appendQueryParameter("business_id", business_id);
		return builder.build().toString();
	}


	public static String updateGCMToken(String business_id,String token) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", "updateGCMToken")
				.appendQueryParameter("business_id", business_id)
				.appendQueryParameter("token", token);
		return builder.build().toString();
	}


	public static String statisticUrl(String tag,String business_id){
		return SERVER_URL+"tag="+tag+"&business_id="+business_id;
	}
    public static String groupUrl(String tag,String business_id){
		return SERVER_URL+"tag="+tag+"&business_id="+business_id;
    }
    public static String feedbackUrl(String tag,String message,String phonenumber){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("message", message)
		       .appendQueryParameter("phone_number", phonenumber);
		return builder.build().toString();
    }

	public static String search(String tag,String name){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("name",name);
		return builder.build().toString();
	}
	public static String faqUrl(String status) {

		return SERVER_URL+"tag="+status;
	}

	public static String sentSMSUrl(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);

		return builder.build().toString();

	}

	public static String userProfile(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder .scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		return builder.build().toString();
	}

	public static String getContacts(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		return builder.build().toString();
	}

	public static String sendDeleteMessageUrl(String tag,String business_id,String message_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id)
		        .appendQueryParameter("message_id",message_id);
		return builder.build().toString();
	}
	public static String addCustomContactUrl(String tag,String name,String email,String phone_number,String business_id,String category_name){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("name",name)
				.appendQueryParameter("email",email)
		        .appendQueryParameter("phone_number",phone_number)
		        .appendQueryParameter("business_id",business_id)
		        .appendQueryParameter("category_name",category_name);
		return builder.build().toString();
	}
	public static String createNewGroupUrl(String tag,String name,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("name",name)
				.appendQueryParameter("business_id",business_id);
		return builder.build().toString();
	}

	public static String getCategoriesUrl(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		return builder.build().toString();
	}
	public static String getContactByCategory(String tag,String business_id,String category_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id)
				.appendQueryParameter("category_id",category_id);
		return builder.build().toString();

	}
	public static String deleteCategoryUrl(String tag,String category,String category_id,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("type",category)
				.appendQueryParameter("category_id",category_id)
		        .appendQueryParameter("business_id",business_id);
		return builder.build().toString();

	}
	public static String deleteContactUrl(String tag,String subscriber_info_id,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.encodedAuthority(AUTHORITY)
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("subscriber_info_id",subscriber_info_id)
				.appendQueryParameter("business_id",business_id);
		return builder.build().toString();

	}
}
