package karibusms.extras;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import karibusms.R;

public  class PrintMessage extends AppCompatActivity {

    public  void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


}
