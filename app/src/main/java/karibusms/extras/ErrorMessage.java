package karibusms.extras;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.File;

import karibusms.logging.L;

/**
 * Created by yohana on 6/29/16.
 */
public class ErrorMessage {

    Context context;
    public ErrorMessage(Context context){
        this.context=context;
    }
    public void handleVolleyError(VolleyError error) {
        if (fileExistence("pspf_swahili")) {
            if (error instanceof TimeoutError) {
                L.m("ERROR MESSAGE"+ error.getMessage());
                L.t(context, "Samahani imetafuta mda mrefu");
            }
            else if (error instanceof NoConnectionError) {
                L.t(context,"Mfumo emeshindwa kuunga,angalia intaneti yako");
                //TODO
            }else if (error instanceof AuthFailureError) {
                L.t(context, "Mfumo umeshindwa kukutambua");
                //TODO
            } else if (error instanceof ServerError) {
                L.t(context, "Samahani mfumo unasumbua");
                //TODO
            } else if (error instanceof NetworkError) {
                L.t(context, "Samahani hauna intaneti");
                //TODO
            } else if (error instanceof ParseError) {
                L.t(context, "Samahani imeshindwa kurudisha taarifa");
                //TODO
            }else{
                //If an error occurs that means end of the list has reached
                L.t(context, "umefika mwisho");

            }
        }else{
            if (error instanceof TimeoutError) {
                L.m("ERROR MESSAGE"+ error.getMessage());
                L.t(context, "Check your connection please");
            } else if (error instanceof NoConnectionError) {
                L.t(context, "No  connection try again please");
                //TODO
            } else if (error instanceof AuthFailureError) {
                L.t(context, "KaribuSMS fails to authenticate");
                //TODO
            } else if (error instanceof ServerError) {
                L.t(context, "Sorry ! There is no connection");
                //TODO
            } else if (error instanceof NetworkError) {
                L.t(context, "Check your internet connection please");
                //TODO
            } else if (error instanceof ParseError) {
                L.t(context, "Problem occured , try after sometime");
                //TODO
            } else {
                L.t(context, "No More Items Available");

            }
        }
    }
    public boolean fileExistence(String filename) {
        File file = context.getFileStreamPath(filename);
        return file.exists();
    }
}
